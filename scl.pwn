#include <a_samp>
#include <a_mysql> //R5 by G-sTyLeZzZ
#include <md5>
#include <zcmd>
#include <sscanf2>
#include <gvar>
#include <PStreamer7>
#include <foreach>



#include "modules/defines.pwn"
#include "modules/variables.pwn"
#include "modules/groups.pwn"
#include "modules/commands_chat.pwn"
#include "modules/object.pwn"
#include "modules/textdraws.pwn"
#include "modules/database.pwn"
#include "modules/doors.pwn"
#include "modules/vehicles.pwn"
#include "modules/stocks.pwn"
#include "modules/items.pwn"
#include "modules/punishes.pwn"




main()
{
	print("\n----------------------------------");
	print(" SC-Life v"VERSION" loaded.");
	print("----------------------------------\n");
}

CallBack:MySQLConnect(sqlhost[], sqluser[], sqlpass[], sqldb[])
{
	print("MYSQL: Attempting to connect to server...");
	mysql_connect(sqlhost, sqluser, sqldb, sqlpass);


	if(mysql_ping())
	{
		print("MYSQL: Database connection established.");
		return 1;
	}
	else
	{
		print("MYSQL: Connection error, retrying...");
		mysql_connect(sqlhost, sqluser, sqldb, sqlpass);
		if(mysql_ping())
		{
			print("MYSQL: Reconnection successful. We can continue as normal.");
			return 1;
		}
		else
		{
			print("MYSQL: Could not reconnect to server, terminating server...");
			SendRconCommand("exit");
			return 0;
		}
	}
}

public OnGameModeInit()
{
	new startTime = GetTickCount();
	
	MySQLConnect(MYSQL_HOST, MYSQL_USER, MYSQL_PASS, MYSQL_DB);
	
	//AntyDeAMX();
	ShowNameTags(0);
	EnableStuntBonusForAll(0);
	DisableInteriorEnterExits();
	AllowInteriorWeapons(1);
	
	mysql_debug(1);
	ChechIfTableExists();
	
	SendRconCommand("weburl www.sc-life.pl");
	SendRconCommand("mapname "CITY_NAME"");
	SetGameModeText("SC-Life: v"VERSION"");
	
	LoadSettings();
	LoadVehicles();
	Objects(); // Only for debug
	CreateTextDraws();
	LoadDoors();
	LoadItems();
	Main_Timer = SetTimer("MainTimer", 1000, true);
	Min_Timer = SetTimer("MinTimer", 60000, true);
	printf("Bayside successfully loaded (in %0.3f seconds)\r\n", float(GetTickCount() - startTime) / 1000);
	return 1;
}

public OnGameModeExit()
{
	foreach(new veh : Vehicle)
    {
		SaveVehicle(Vehicle[veh][vUID]);
		DestroyVehicle(veh);
		Iter_Remove(Vehicle, veh);
		for(new v:i; i != v; i++) Vehicle[veh][i] = 0;
    }
	KillTimer(Main_Timer);
	KillTimer(Min_Timer);
	return 1;
}

public OnPlayerRequestClass(playerid, classid)
{
	SetPlayerCameraPos(playerid, -2323.59, 2125.24, 62.95);
	SetPlayerCameraLookAt(playerid, -2422.9106, 2326.8025, 4.9823);
	SpawnPlayer(playerid);
	return 1;
}

public OnPlayerConnect(playerid)
{
	/*RemoveBuildingForPlayer(playerid, 9260, -2291.6094, 2311.5313, 9.0938, 0.25);
	RemoveBuildingForPlayer(playerid, 9301, -2530.3516, 2346.2031, 7.9688, 0.25);
	RemoveBuildingForPlayer(playerid, 9373, -2530.3516, 2346.2031, 7.9688, 0.25);	
	RemoveBuildingForPlayer(playerid, 9380, -2291.6094, 2311.5313, 9.0938, 0.25);	
	RemoveBuildingForPlayer(playerid, 9381, -2235.5547, 2361.7734, 15.8047, 0.25);
	RemoveBuildingForPlayer(playerid, 9382, -2251.6484, 2380.0938, 10.5000, 0.25);
	RemoveBuildingForPlayer(playerid, 1346, -2550.3672, 2320.4766, 5.3203, 0.25);
	RemoveBuildingForPlayer(playerid, 1346, -2553.0547, 2320.4844, 5.3203, 0.25);
	RemoveBuildingForPlayer(playerid, 1617, -2548.9922, 2352.3359, 11.1484, 0.25);
	RemoveBuildingForPlayer(playerid, 1689, -2544.5547, 2348.0156, 13.2422, 0.25);
	RemoveBuildingForPlayer(playerid, 1617, -2539.6172, 2352.3359, 11.1484, 0.25);
	RemoveBuildingForPlayer(playerid, 1308, -2531.3438, 2336.8516, 4.2031, 0.25);
	RemoveBuildingForPlayer(playerid, 1227, -2527.2422, 2353.1250, 4.7578, 0.25);
	RemoveBuildingForPlayer(playerid, 1227, -2520.7188, 2353.1250, 4.7578, 0.25);
	RemoveBuildingForPlayer(playerid, 1227, -2524.0625, 2353.1250, 4.7578, 0.25);
	RemoveBuildingForPlayer(playerid, 1297, -2510.5469, 2335.7578, 7.3750, 0.25);
	RemoveBuildingForPlayer(playerid, 1440, -2503.3125, 2341.3672, 4.4531, 0.25);
	RemoveBuildingForPlayer(playerid, 1346, -2451.4063, 2321.0234, 5.3203, 0.25);
	RemoveBuildingForPlayer(playerid, 715, -2511.0469, 2510.0313, 25.6484, 0.25);
	RemoveBuildingForPlayer(playerid, 1297, -2279.3750, 2327.1484, 3.9531, 0.25);
	RemoveBuildingForPlayer(playerid, 1635, -2226.0625, 2360.8281, 6.3984, 0.25);
	RemoveBuildingForPlayer(playerid, 1440, -2244.2344, 2361.2031, 4.4453, 0.25);
	RemoveBuildingForPlayer(playerid, 1264, -2247.6328, 2364.8594, 4.3828, 0.25);
	RemoveBuildingForPlayer(playerid, 1264, -2246.7734, 2364.4922, 4.3828, 0.25);
	RemoveBuildingForPlayer(playerid, 1264, -2246.8125, 2365.7578, 4.3828, 0.25);
	RemoveBuildingForPlayer(playerid, 1431, -2245.7109, 2363.3047, 4.5000, 0.25);
	RemoveBuildingForPlayer(playerid, 9245, -2235.5547, 2361.7734, 15.8047, 0.25);
	RemoveBuildingForPlayer(playerid, 1227, -2253.5391, 2372.5469, 4.7578, 0.25);
	RemoveBuildingForPlayer(playerid, 1264, -2254.0859, 2371.0313, 4.3828, 0.25);
	RemoveBuildingForPlayer(playerid, 1264, -2252.5391, 2371.0234, 4.3828, 0.25);
	RemoveBuildingForPlayer(playerid, 9247, -2251.6484, 2380.0938, 10.5000, 0.25);
	RemoveBuildingForPlayer(playerid, 1431, -2254.7969, 2385.4609, 4.5000, 0.25);*/
	RemoveBuildingForPlayer(playerid, 9260, -2291.6094, 2311.5313, 9.0938, 0.25);
	RemoveBuildingForPlayer(playerid, 9301, -2530.3516, 2346.2031, 7.9688, 0.25);
	RemoveBuildingForPlayer(playerid, 9330, -2481.0000, 2497.6797, 16.3438, 0.25);
	RemoveBuildingForPlayer(playerid, 9373, -2530.3516, 2346.2031, 7.9688, 0.25);
	RemoveBuildingForPlayer(playerid, 9380, -2291.6094, 2311.5313, 9.0938, 0.25);
	RemoveBuildingForPlayer(playerid, 9381, -2235.5547, 2361.7734, 15.8047, 0.25);
	RemoveBuildingForPlayer(playerid, 9382, -2251.6484, 2380.0938, 10.5000, 0.25);
	RemoveBuildingForPlayer(playerid, 9411, -2418.3281, 2454.8438, 18.4063, 0.25);
	RemoveBuildingForPlayer(playerid, 9412, -2477.2578, 2455.9609, 21.5391, 0.25);
	RemoveBuildingForPlayer(playerid, 9414, -2461.8672, 2487.3359, 17.5781, 0.25);	
	RemoveBuildingForPlayer(playerid, 9419, -2423.5859, 2488.2734, 13.7344, 0.25);
	RemoveBuildingForPlayer(playerid, 9432, -2481.0000, 2497.6797, 16.3438, 0.25);
	RemoveBuildingForPlayer(playerid, 9434, -2476.8672, 2454.7109, 15.6016, 0.25);
	RemoveBuildingForPlayer(playerid, 1346, -2550.3672, 2320.4766, 5.3203, 0.25);
	RemoveBuildingForPlayer(playerid, 1346, -2553.0547, 2320.4844, 5.3203, 0.25);
	RemoveBuildingForPlayer(playerid, 1617, -2548.9922, 2352.3359, 11.1484, 0.25);
	RemoveBuildingForPlayer(playerid, 1689, -2544.5547, 2348.0156, 13.2422, 0.25);
	RemoveBuildingForPlayer(playerid, 1617, -2539.6172, 2352.3359, 11.1484, 0.25);
	RemoveBuildingForPlayer(playerid, 1308, -2531.3438, 2336.8516, 4.2031, 0.25);
	RemoveBuildingForPlayer(playerid, 1227, -2527.2422, 2353.1250, 4.7578, 0.25);
	RemoveBuildingForPlayer(playerid, 1227, -2520.7188, 2353.1250, 4.7578, 0.25);
	RemoveBuildingForPlayer(playerid, 1227, -2524.0625, 2353.1250, 4.7578, 0.25);
	RemoveBuildingForPlayer(playerid, 1297, -2510.5469, 2335.7578, 7.3750, 0.25);
	RemoveBuildingForPlayer(playerid, 1440, -2503.3125, 2341.3672, 4.4531, 0.25);
	RemoveBuildingForPlayer(playerid, 1346, -2451.4063, 2321.0234, 5.3203, 0.25);
	RemoveBuildingForPlayer(playerid, 715, -2511.0469, 2510.0313, 25.6484, 0.25);
	RemoveBuildingForPlayer(playerid, 647, -2475.1406, 2446.0625, 16.3984, 0.25);
	RemoveBuildingForPlayer(playerid, 9324, -2477.2578, 2455.9609, 21.5391, 0.25);
	RemoveBuildingForPlayer(playerid, 9331, -2476.8672, 2454.7109, 15.6016, 0.25);
	RemoveBuildingForPlayer(playerid, 647, -2468.0391, 2450.6875, 16.5703, 0.25);
	RemoveBuildingForPlayer(playerid, 9238, -2461.8672, 2487.3359, 17.5781, 0.25);
	RemoveBuildingForPlayer(playerid, 1358, -2462.1484, 2512.7422, 16.9922, 0.25);
	RemoveBuildingForPlayer(playerid, 1264, -2461.1016, 2507.9688, 16.2969, 0.25);
	RemoveBuildingForPlayer(playerid, 1264, -2459.7031, 2508.6719, 16.2969, 0.25);
	RemoveBuildingForPlayer(playerid, 1264, -2461.1016, 2509.3828, 16.2969, 0.25);
	RemoveBuildingForPlayer(playerid, 1264, -2460.1406, 2509.7422, 16.2969, 0.25);
	RemoveBuildingForPlayer(playerid, 9319, -2418.3281, 2454.8438, 18.4063, 0.25);
	RemoveBuildingForPlayer(playerid, 9323, -2423.5859, 2488.2734, 13.7344, 0.25);
	RemoveBuildingForPlayer(playerid, 1297, -2279.3750, 2327.1484, 3.9531, 0.25);
	RemoveBuildingForPlayer(playerid, 1635, -2226.0625, 2360.8281, 6.3984, 0.25);
	RemoveBuildingForPlayer(playerid, 1440, -2244.2344, 2361.2031, 4.4453, 0.25);
	RemoveBuildingForPlayer(playerid, 1264, -2247.6328, 2364.8594, 4.3828, 0.25);
	RemoveBuildingForPlayer(playerid, 1264, -2246.7734, 2364.4922, 4.3828, 0.25);
	RemoveBuildingForPlayer(playerid, 1264, -2246.8125, 2365.7578, 4.3828, 0.25);
	RemoveBuildingForPlayer(playerid, 1431, -2245.7109, 2363.3047, 4.5000, 0.25);
	RemoveBuildingForPlayer(playerid, 9245, -2235.5547, 2361.7734, 15.8047, 0.25);
	RemoveBuildingForPlayer(playerid, 1227, -2253.5391, 2372.5469, 4.7578, 0.25);	
	RemoveBuildingForPlayer(playerid, 1264, -2254.0859, 2371.0313, 4.3828, 0.25);
	RemoveBuildingForPlayer(playerid, 1264, -2252.5391, 2371.0234, 4.3828, 0.25);
	RemoveBuildingForPlayer(playerid, 9247, -2251.6484, 2380.0938, 10.5000, 0.25);
	RemoveBuildingForPlayer(playerid, 1431, -2254.7969, 2385.4609, 4.5000, 0.25);
	
	
	/*drzwitd = CreatePlayerTextDraw(playerid, 445.500000, 196.583358, "~b~Drzwi ~w~Testowe~n~~n~~g~Koszt wejscia: ~w~ 20$~n~Wcisnij klawisz ~y~sprintu~w~ aby wejsc");
	PlayerTextDrawLetterSize(playerid, drzwitd, 0.331499, 1.156666);
	PlayerTextDrawTextSize(playerid, drzwitd, 641.500000, 28.000000);
	PlayerTextDrawAlignment(playerid, drzwitd, 1);
	PlayerTextDrawColor(playerid, drzwitd, -1);
	PlayerTextDrawUseBox(playerid, drzwitd, true);
	PlayerTextDrawBoxColor(playerid, drzwitd, 120);
	PlayerTextDrawSetShadow(playerid, drzwitd, 0);
	PlayerTextDrawSetOutline(playerid, drzwitd, 1);
	PlayerTextDrawBackgroundColor(playerid, drzwitd, 51);
	PlayerTextDrawFont(playerid, drzwitd, 1);
	PlayerTextDrawSetProportional(playerid, drzwitd, 1);*/
	
	drzwitd = CreatePlayerTextDraw(playerid, 326.000000, 330.000000, "_");
	PlayerTextDrawAlignment(playerid, drzwitd, 2);
	PlayerTextDrawBackgroundColor(playerid, drzwitd, 255);
	PlayerTextDrawFont(playerid, drzwitd, 1);
	PlayerTextDrawLetterSize(playerid, drzwitd, 0.379999, 1.299999);
	PlayerTextDrawColor(playerid, drzwitd, -1);
	PlayerTextDrawSetOutline(playerid, drzwitd, 1);
	PlayerTextDrawSetProportional(playerid, drzwitd, 1);
	PlayerTextDrawUseBox(playerid, drzwitd, 1);
	PlayerTextDrawBoxColor(playerid, drzwitd, 68);
	PlayerTextDrawTextSize(playerid, drzwitd, 490.000000, 221.000000);
	
	Player[playerid][WeaponID] = 0;
	Player[playerid][WeaponUID] = 0;

	new query[128], string[412];
	if(IsPlayerNPC(playerid)) return Kick(playerid);
	
	GetPlayerName(playerid, Player[playerid][GlobalName], MAX_PLAYER_NAME);
	
	format(query, sizeof(query), "SELECT `name`, `member_id`, `members_pass_salt` FROM members WHERE name = '%s' LIMIT 1", Player[playerid][GlobalName]);
    mysql_query(query);
    mysql_store_result();
    
   	if(mysql_num_rows() != 0)
   	{
		mysql_fetch_row_format(query);
	    sscanf(query, "p<|>{s[128]}ds[12]", Player[playerid][GUID], Player[playerid][Salt]);
	    mysql_free_result();
		printf("%d, %s", Player[playerid][GUID], Player[playerid][Salt]);
   	    format(string, sizeof(string), "{336699}Witaj na "longerservername", nowym serwerze z zasadami.\nStawiamy poprzeczk� wy�ej dla siebie i dla konkurencji!{a9c4e4}\n\nKonto o nicku {ffffff}%s{a9c4e4} ju� istnieje.\n", Player[playerid][GlobalName]);
		strcat(string, "1.\tJest Twoje? Wprowad� has�o i zaloguj si�!\n2.\tNie posiadasz konta globalnego? Wejd� na "www"!\n3.\tKonto nie jest Twoje? Kliknij {ffffff}ZMIE�.");
        ShowPlayerDialog(playerid, DIALOG_LOGIN_GLOBAL, DIALOG_STYLE_PASSWORD, "{4876FF}"longerservername"{a9c4e4} � Logowanie", string, "Zaloguj", "ZMIE�");
   	}
   	else
   	{
   	    format(string, sizeof(string), "{336699}Witaj na "longerservername", nowym serwerze z zasadami.\nStawiamy poprzeczk� wy�ej dla siebie i dla konkurencji!{a9c4e4}\n\nKonto o nicku {ffffff}%s{a9c4e4} nie istnieje.\n", Player[playerid][GlobalName]);
		strcat(string, "1.\tNie posiadasz konta globalnego? Wejd� na "www"!\n2.\tKonto nie jest Twoje? Kliknij {ffffff}ZMIE�.");
        ShowPlayerDialog(playerid, DIALOG_LOGIN, DIALOG_STYLE_PASSWORD, "{4876FF}"longerservername"{a9c4e4} � Logowanie", string, "Zmie�", "ZMIE�");
   	}
   	mysql_free_result();
	return 1;
}

public OnPlayerDisconnect(playerid, reason)
{
	if(reason == 0)
 	{
  		SendMessageToDrivers(playerid, "W�a�ciciel tego pojazdu opu�ci� gr� w wyniku crasha. Pojazd b�dzie dostepny przez nast�pne 3 minuty");
		SendMessageToDrivers(playerid, "i zniknie je�li jego w�a�ciciel nie wr�ci w tym czasie do gry.");
  		SetTimerEx("UnSpawnVehiclesIf", 180000, 0, "d", Player[playerid][UID]);
 	}
 	else
 	{
  		SendMessageToDrivers(playerid, "W�a�ciciel tego pojazdu opu�ci� gr�.. Pojazd b�dzie dostepny przez nast�pn� minut�");
		SendMessageToDrivers(playerid, "i zniknie je�li jego w�a�ciciel nie wr�ci w tym czasie do gry.");
  		SetTimerEx("UnSpawnVehiclesIf", 60000, 0, "d", Player[playerid][UID]);
  	}
	
	Delete3DTextLabel(Player[playerid][Nick]);
	OnPlayerSave(playerid);
	
	for(new pInfo:i; i != pInfo; i++)
    {
 		Player[playerid][i] = 0;
 	}
	return 1;
}

public OnPlayerSpawn(playerid)
{
	if(!Player[playerid][Logged]) return Kick(playerid);
	
	if(Player[playerid][AdminJail] > 0)
	{
		SetPlayerPos(playerid, 154.1221, -1951.9156, 47.8750);
		SetPlayerInterior(playerid, 0);
		SetPlayerVirtualWorld(playerid, playerid + 1000);
	}
	else
	{
		SetCameraBehindPlayer(playerid);
		SetPlayerPos(playerid, Settings[Spawn][0], Settings[Spawn][1], Settings[Spawn][2]);
		SetPlayerInterior(playerid, Settings[SpawnInt]);
		SetPlayerFacingAngle(playerid, Settings[Spawn][3]);
		SetPlayerVirtualWorld(playerid, Settings[SpawnVw]);
	}
	//SetPlayerSkin(playerid, Player[playerid][Skin]);
	Spawned[playerid] = 1;
	return 1;
}

public OnPlayerDeath(playerid, killerid, reason)
{
	
	return 1;
}

public OnVehicleSpawn(vehicleid)
{
	return 1;
}

public OnVehicleDeath(vehicleid, killerid)
{
	return 1;
}

public OnPlayerText(playerid, text[])
{
	if(strlen(text) < 3)
	{
		if((strcmp(":)", text, true, strlen(text)) == 0) && (strlen(text) == strlen(":)")))
		{
			ServerMe(playerid, "u�miecha si�.");
			return 0;
		}
		else if((strcmp(":/", text, true, strlen(text)) == 0) && (strlen(text) == strlen(":/")))
		{
			ServerMe(playerid, "krzywi si�.");
			return 0;
		}
		else if((strcmp(":(", text, true, strlen(text)) == 0) && (strlen(text) == strlen(":(")))
		{
			ServerMe(playerid, "robi smutn� min�.");
			return 0;
		}
		else if((strcmp(":o", text, true, strlen(text)) == 0) && (strlen(text) == strlen(":o")))
		{
			ServerMe(playerid, "robi wielkie oczy.");
			return 0;
		}
		else if((strcmp(":*", text, true, strlen(text)) == 0) && (strlen(text) == strlen(":*")))
		{
			ApplyAnimation(playerid, "KISSING", "Playa_Kiss_01", 4.1, 0, 0, 0, 0, -1);
			return 0;
		}
		else if((strcmp(":D", text, true, strlen(text)) == 0) && (strlen(text) == strlen(":D")))
		{
			ApplyAnimation(playerid, "RAPPING", "Laugh_01", 4.1, 0, 0, 0, 0, 0);
			ServerMe(playerid, "�mieje si�.");
			return 0;
		}
	}
	if(text[0] == '$')
	{
		OnPlayerCommandText(playerid, text);
		return 0;
	}
	if(text[0] == '!')
	{
		ChatGroups(playerid, text);
		return 0;
	}
	
	if(Player[playerid][CallingTo] != INVALID_PLAYER_ID)
	{
	    /*if(PlayerCache[playerid][pCallingTo] == 911 || PlayerCache[playerid][pCallingTo] == 333 || PlayerCache[playerid][pCallingTo] == 777 || PlayerCache[playerid][pCallingTo] == 444)
	    {
	        return 0;
	    }*/
		new string[128];
	    new called_player = Player[playerid][CallingTo];
	    if(!Player[called_player][Logged] || !Spawned[called_player] || Player[called_player][CallingTo] != playerid)
	    {
	        Player[playerid][CallingTo] = INVALID_PLAYER_ID;
	        
	        SendClientMessage(playerid, COLOR_YELLOW, "Utracono po��czenie z rozm�wc�.");
	        SetPlayerSpecialAction(playerid, SPECIAL_ACTION_STOPUSECELLPHONE);
	        RemovePlayerAttachedObject(playerid, SLOT_PHONE);
	        return 0;
	    }
	    format(string, sizeof(string), "[Telefon]: %s", text);
	    SendClientMessage(called_player, COLOR_YELLOW, string);
	    
   		format(string, sizeof(string), "(telefon): %s", text);
  		SendClientMessageEx(10.0, playerid, string, COLOR_FADE1, COLOR_FADE2, COLOR_FADE3, COLOR_FADE4, COLOR_FADE5);
	    return 0;
	}	
	
	new string[128];
	ucfirst(text);
	capitalize(text);
	
	if(strlen(text) > SPLIT_TEXT_LIMIT)
	{
		new stext[128];

		strmid(stext, text, SPLIT_TEXT1_FROM, SPLIT_TEXT1_TO, 255);
		format(string, sizeof(string), "%s m�wi: %s...", Player[playerid][NameSpace], stext);
		SendClientMessageEx(6.5, playerid, string, COLOR_FADE1, COLOR_FADE2, COLOR_FADE3, COLOR_FADE4, COLOR_FADE5);

		strmid(stext, text, SPLIT_TEXT2_FROM, SPLIT_TEXT2_TO, 255);
		format(string, sizeof(string), "%s m�wi: ...%s", Player[playerid][NameSpace], stext);
		SendClientMessageEx(6.5, playerid, string, COLOR_FADE1, COLOR_FADE2, COLOR_FADE3, COLOR_FADE4, COLOR_FADE5);
	}
	else
	{
		format(string, sizeof(string), "%s m�wi: %s", Player[playerid][NameSpace], text);
		SendClientMessageEx(6.5, playerid, string, COLOR_FADE1, COLOR_FADE2, COLOR_FADE3, COLOR_FADE4, COLOR_FADE5);
	}
	return 0;
}

public OnPlayerCommandText(playerid, cmdtext[])
{
	if(8 >= strval(cmdtext[1]) > 0) return ChatGroups(playerid, cmdtext);	
	
	return 0;
}

public OnPlayerEnterVehicle(playerid, vehicleid, ispassenger)
{
	return 1;
}

public OnPlayerExitVehicle(playerid, vehicleid)
{
	return 1;
}

public OnPlayerStateChange(playerid, newstate, oldstate)
{
	new veh = GetPlayerVehicleID(playerid);

    if(newstate == PLAYER_STATE_DRIVER && oldstate == PLAYER_STATE_ONFOOT && !Vehicle[veh][vEngine])
	{
		TextDrawShowForPlayer(playerid, EngineVehicles);
	}
	
	if(newstate == PLAYER_STATE_ONFOOT && oldstate == PLAYER_STATE_DRIVER) return TextDrawHideForPlayer(playerid, EngineVehicles);

	return 1;
}

public OnPlayerEnterCheckpoint(playerid)
{
	if(Player[playerid][CheckPoint] == 1)
	{
		DisablePlayerCheckpoint(playerid);
		Player[playerid][CheckPoint] = 0;
	}
	return 1;
}

public OnPlayerLeaveCheckpoint(playerid)
{
	return 1;
}

public OnPlayerEnterRaceCheckpoint(playerid)
{
	return 1;
}

public OnPlayerLeaveRaceCheckpoint(playerid)
{
	return 1;
}

public OnRconCommand(cmd[])
{
	return 1;
}

public OnPlayerRequestSpawn(playerid)
{
	return 1;
}

public OnObjectMoved(objectid)
{
	return 1;
}

public OnPlayerObjectMoved(playerid, objectid)
{
	return 1;
}

public OnPlayerPickUpPickup(playerid, pickupid)
{
	foreach(new id : Doors)
	{
		if(pickupid == DoorCache[id][dPickupID])
		{
			ShowDoorTextDraw(playerid, id);
		}
	}
	return 1;
}

public OnVehicleMod(playerid, vehicleid, componentid)
{
	return 1;
}

public OnVehiclePaintjob(playerid, vehicleid, paintjobid)
{
	return 1;
}

public OnVehicleRespray(playerid, vehicleid, color1, color2)
{
	return 1;
}

public OnPlayerSelectedMenuRow(playerid, row)
{
	return 1;
}

public OnPlayerExitedMenu(playerid)
{
	return 1;
}

public OnPlayerInteriorChange(playerid, newinteriorid, oldinteriorid)
{
	return 1;
}

public OnPlayerKeyStateChange(playerid, newkeys, oldkeys)
{
	if(IsKeyJustDown(KEY_HANDBRAKE,newkeys,oldkeys))
	{
		if(GetPlayerSpecialAction(playerid) != SPECIAL_ACTION_NONE && GetPlayerSpecialAction(playerid) != SPECIAL_ACTION_DRINK_BEER
		&& GetPlayerSpecialAction(playerid) != SPECIAL_ACTION_DRINK_WINE) return SetPlayerSpecialAction(playerid, SPECIAL_ACTION_NONE);
		ApplyAnimation(playerid, "CARRY", "crry_prtial", 4.0, 0, 0, 0, 0, 0);
	}
	if(PRESSED(KEY_ACTION + KEY_FIRE)) return cmd_v(playerid, "silnik");
	
	if((newkeys==KEY_SPRINT + KEY_WALK)&&(GetPlayerState(playerid)==PLAYER_STATE_ONFOOT))
	{
		new i;
		foreach(i : Doors)
		{
		    if(GetPlayerVirtualWorld(playerid) == DoorCache[i][dEnterVW])
		    {
		    	if(IsPlayerInRangeOfPoint(playerid, 2.0, DoorCache[i][dEnterX], DoorCache[i][dEnterY], DoorCache[i][dEnterZ]))
				{
		    	    OnPlayerEnterDoors(playerid, i);
					return 1;
				}	
			}
			if(Player[playerid][Door] == i)
			{
			    if(IsPlayerInRangeOfPoint(playerid, 2.0, DoorCache[i][dExitX], DoorCache[i][dExitY], DoorCache[i][dExitZ]))
				{
			        OnPlayerExitDoors(playerid, i);
					return 1;
				}
			}
		}
	}
	return 1;
}

public OnRconLoginAttempt(ip[], password[], success)
{
	return 1;
}

stock GetWeaponType(weaponid)
{
	switch(weaponid)
 	{
  		case 22, 23, 24, 26, 28, 32:
    		return WEAPON_TYPE_LIGHT;

		case 3, 4, 16, 17, 18, 39, 10, 11, 12, 13, 14, 40, 41:
  			return WEAPON_TYPE_MELEE;

		case 2, 5, 6, 7, 8, 9, 25, 27, 29, 30, 31, 33, 34, 35, 36, 37, 38:
  			return WEAPON_TYPE_HEAVY;
    }
    return WEAPON_TYPE_NONE;
}

public OnPlayerUpdate(playerid)
{
	if(Player[playerid][WeaponUID])
	{
	    new weapon = GetPlayerWeapon(playerid);
		if(weapon == 0)
		{
		    new weapon_id = Player[playerid][WeaponID];
  			if(!IsPlayerAttachedObjectSlotUsed(playerid, SLOT_WEAPON))
  			{
  			    new weapon_type = GetWeaponType(weapon_id);
  			    switch(weapon_type)
  			    {
  			        case WEAPON_TYPE_LIGHT:
  			        {
  			            SetPlayerAttachedObject(playerid, SLOT_WEAPON, WeaponModel[weapon_id], 8, 0.0, -0.1, 0.15, -100.0, 0.0, 0.0);
  			        }
  			        case WEAPON_TYPE_MELEE:
  			        {
  			            SetPlayerAttachedObject(playerid, SLOT_WEAPON, WeaponModel[weapon_id], 7, 0.0, 0.0, -0.18, 100.0, 45.0, 0.0);
  			        }
  			        case WEAPON_TYPE_HEAVY:
  			        {
  			            SetPlayerAttachedObject(playerid, SLOT_WEAPON, WeaponModel[weapon_id], 1, 0.2, -0.125, -0.1, 0.0, 25.0, 180.0);
  			        }
  			    }
			}
		}
		else
		{
  			if(IsPlayerAttachedObjectSlotUsed(playerid, SLOT_WEAPON))
    		{
				RemovePlayerAttachedObject(playerid, SLOT_WEAPON);
			}
		}
	}
	return 1;
}

public OnPlayerStreamIn(playerid, forplayerid)
{
	return 1;
}

public OnPlayerStreamOut(playerid, forplayerid)
{
	return 1;
}

public OnVehicleStreamIn(vehicleid, forplayerid)
{
	return 1;
}

public OnVehicleStreamOut(vehicleid, forplayerid)
{
	return 1;
}

public OnDialogResponse(playerid, dialogid, response, listitem, inputtext[])
{
	if(dialogid == DIALOG_LOGIN_GLOBAL)
	{
		if(!response) return 1;
		new password[64], query[512], pass[256], string[256];
		
		#pragma unused pass

		mysql_real_escape_string(inputtext, password);
		format(pass, sizeof(pass), "%s%s", MD5_Hash(Player[playerid][Salt]), MD5_Hash(password));
		format(query, sizeof(query), "SELECT `member_id` FROM  members WHERE members_pass_hash = md5('%s') AND member_id = %d", pass, Player[playerid][GUID]);
		//format(query, sizeof(query), "SELECT `member_id` FROM  members WHERE haslo = '%s' AND member_id = %d", inputtext, Player[playerid][GUID]);
		mysql_query(query);
		mysql_store_result();

		if(mysql_num_rows())
		{
			mysql_free_result();
			
			format(query, sizeof(query), "SELECT `nick`, `id` FROM  "prefix"user_data WHERE  `guid` = '%d'", Player[playerid][GUID]);
			mysql_query(query);
			mysql_store_result();

			if(mysql_num_rows())
			{
				while(mysql_fetch_row_format(query, "|"))
				{
					sscanf(query, "p<|>s[32]d",
					Player[playerid][Name],
					Player[playerid][UID]);
					
					format(Player[playerid][NameSpace], MAX_PLAYER_NAME, Player[playerid][Name]);
					
					UnderscoreToSpace(Player[playerid][NameSpace]);
					format(string, sizeof(string), "%s%d\t%s\n", string, Player[playerid][UID], Player[playerid][NameSpace]);
				}
				ShowPlayerDialog(playerid, DIALOG_LOGIN, DIALOG_STYLE_LIST, "{4876FF}"longerservername"{a9c4e4} � Logowanie", string, "Wybierz", "");
			}
			else
			{		
				format(string, sizeof(string), "Pod to konto globalne nie jest przypisana {ffffff}�adna{a9c4e4} posta�!\n\n1.\tChcesz stworzy� posta�? Kliknij {ffffff}STW�RZ.{a9c4e4}\n2.\tKliknij {ffffff}wyjd�{a9c4e4}, by opu�ci� gre.");
				ShowPlayerDialog(playerid, DIALOG_NO_CHARACTERS, DIALOG_STYLE_MSGBOX, "{4876FF}"longerservername"{a9c4e4} � Logowanie", string, "STW�RZ", "Wyjd�");
			}	
			mysql_free_result();
	    }
	    else
	    {
	     	ShowPlayerDialog(playerid, DIALOG_LOGIN_GLOBAL, DIALOG_STYLE_PASSWORD, "{4876FF}"longerservername"{a9c4e4} � Logowanie", "Podane has�o nie jest zgodne z tym, zapisanym w bazie danych.", "Zaloguj", "Wyjd�");
	    }
	}
	
	if(dialogid == DIALOG_LOGIN)
	{
        OnPlayerLogin(playerid, strval(inputtext));
	}
	
	if(dialogid == DIALOG_CREATE_GROUPS_1)
	{
		if(!response) return 1;
		
		new name[32];
		mysql_real_escape_string(inputtext, name);
		SetGVarString("create_groups_name", name, playerid);
		ShowPlayerDialog(playerid, DIALOG_CREATE_GROUPS_2, DIALOG_STYLE_INPUT, "{4876FF}"servername"{a9c4e4} � Admin � Grupy", "Pod tag nowej grupy:", "Dalej", "Zamknij");
	}
	
	if(dialogid == DIALOG_CREATE_GROUPS_2)
	{
		if(!response) return 1;
		
		new tag[10];
		mysql_real_escape_string(inputtext, tag);
		SetGVarString("create_groups_tag", tag, playerid);
		ShowPlayerDialog(playerid, DIALOG_CREATE_GROUPS_3, DIALOG_STYLE_INPUT, "{4876FF}"servername"{a9c4e4} � Admin � Grupy", "Podaj kolor nowej grupy:", "Dalej", "Zamknij");
	}
	
	if(dialogid == DIALOG_CREATE_GROUPS_3)
	{
		if(!response) return 1;
		
		new color[10];
		mysql_real_escape_string(inputtext, color);
		SetGVarString("create_groups_color", color, playerid);
		ShowPlayerDialog(playerid, DIALOG_CREATE_GROUPS_4, DIALOG_STYLE_LIST, "{4876FF}"servername"{a9c4e4} � Admin � Grupy", "{6633ff}#Wybierz typ nowej grupy{a9c4e4}\n\n1.PD\n2.SAM-ERS\n3.Gang", "Stw�rz", "Zamknij");
	}
	
	if(dialogid == DIALOG_CREATE_GROUPS_4)
	{
		if(!response) return 1;
		
		new color[10], tag[10], name[32], query[128], string[64];
		
		GetGVarString("create_groups_name", name, sizeof(name), playerid);
		GetGVarString("create_groups_tag", tag, sizeof(tag), playerid);
		GetGVarString("create_groups_color", color, sizeof(color), playerid);
		
		if(strval(inputtext) == 0) return SendClientMessage(playerid, COLOR_GRAD1, "Wyst�pi� b��d podczas tworzenia grupy(ERROR: #001)");

		format(query, sizeof(query), "INSERT INTO "prefix"groups(`name`, `tag`, `color`, `type`) VALUES ( '%s', '%s', '%s', '%d')", name, tag, color, strval(inputtext));
		mysql_query(query);
		
		format(string, sizeof(string), "Utworzy�e� now� grupe: %s.", name);
		SendClientMessage(playerid, COLOR_LIGHTGREEN, string);
	}
	if(dialogid == DIALOG_GROUPS_LIST_1)
	{
		if(!response) return 1;
		new slot = strval(inputtext), title[64];
		SetPVarInt(playerid, "group_list_slot", slot);
		
		format(title, sizeof(title), "{4876FF}Twoje grupy{a9c4e4} � %s (UID: %d)", Group[playerid][slot][gName], Group[playerid][slot][gUID]);
		if(Group[playerid][slot][gUID] > 0) ShowPlayerDialog(playerid, DIALOG_GROUPS_LIST_2, DIALOG_STYLE_LIST, title, "1. On-line\n2. Informacje o grupie\n3. Pojazdy\n4. Posiad�o�ci", "Wybierz", "Zamknij");
		return 1;
	}
	if(dialogid == DIALOG_GROUPS_LIST_2)
	{
		new slot = GetPVarInt(playerid, "group_list_slot"), string[256], title[64];
		switch(listitem)
		{
			case 0: //0nline
			{
				format(title, sizeof(title), "{4876FF}Twoje grupy{a9c4e4} � %s (UID: %d)", Group[playerid][slot][gName], Group[playerid][slot][gUID]);
				format(string, sizeof(string), "{a9c4e4}Lp.\tNazwa(ID){ffffff}\n%s", string);
				for(new i = 0; i < MAX_PLAYERS; i++)
				{
					if(IsPlayerConnected(i))
					{
						for(new id = 1; id < MAX_GROUPS; id++) 
						{
							if(Group[i][id][gUID] == Group[playerid][slot][gUID]) format(string, sizeof(string), "%s%d\t%s(%d)\n", string, i + 1, Player[i][NameSpace], i);
						}
					}
				}	
				ShowPlayerDialog(playerid, DIALOG_GROUPS_LIST_3, DIALOG_STYLE_LIST, title, string, "Zamknij", "");
				return 1;
			}
			case 1: 
			{
				format(title, sizeof(title), "{4876FF}Twoje grupy{a9c4e4} � %s (UID: %d)", Group[playerid][slot][gName], Group[playerid][slot][gUID]);
				format(string, sizeof(string), "{a9c4e4}Opcja\tDane{ffffff}\n\
				Nazwa grupy:\t%s\n\
				Cz�onk�w:\t%d", Group[playerid][slot][gName], CountGroupMembers(Group[playerid][slot][gUID]));
				BSRPDialog(playerid, DIALOG_GROUPS_LIST_3, DIALOG_STYLE_MSGBOX, "Grupy", "Informacje o grupie", string, "Ok", " ");
			}
		}
		return 1;
	}
	if(dialogid == DIALOG_DOORS_CREATE1)
	{
		if(!response) return 1;
		SetPVarString(playerid, "TworzenieDrzwiNazwa", inputtext);
		ShowPlayerDialog(playerid, DIALOG_DOORS_CREATE2, DIALOG_STYLE_LIST, "{4876FF}Drzwi{a9c4e4} � Tworzenie", "1. Drzwi\n2. Dom\n3. Bankomat", "Dalej", "Anuluj");
	}
	if(dialogid == DIALOG_DOORS_CREATE2)
	{
		if(!response) return 1;
		SetPVarInt(playerid, "TworzenieDrzwiTyp", strval(inputtext));
		new str[256];
		format(str, 256, "Nazwa: %s\nVirtual World(Zewn�trzny): %d\nVirtual World(Wewn�trzny): %d", inputtext, GetPVarInt(playerid, "TworzenieDrzwiVW2"), GetPVarInt(playerid, "TworzenieDrzwiVW"));
		ShowPlayerDialog(playerid, DIALOG_DOORS_CREATE3, DIALOG_STYLE_MSGBOX, "{4876FF}Drzwi{a9c4e4} � Tworzenie", str, "Akceptuj", "Anuluj");		
	}
	if(dialogid == DIALOG_DOORS_CREATE2)
	{
		if(!response) return 1;
		new str[256];
		new str2[64];
		GetPVarString(playerid, "TworzenieDrzwiNazwa", str2, 64);
		format(str, 256, "Nazwa: %s\nVirtual World(Zewn�trzny): %d\nVirtual World(Wewn�trzny): %d", inputtext, GetPVarInt(playerid, "TworzenieDrzwiVW2"), GetPVarInt(playerid, "TworzenieDrzwiVW"));
		CreateDoors(str2, GetPVarInt(playerid, "TworzenieDrzwiTyp"), GetPVarInt(playerid, "TworzenieDrzwiVW2"), GetPVarInt(playerid, "TworzenieDrzwiInt2"),
		GetPVarFloat(playerid, "TworzenieDrzwiX2"), GetPVarFloat(playerid, "TworzenieDrzwiY2"), GetPVarFloat(playerid, "TworzenieDrzwiZ2"), GetPVarInt(playerid, "TworzenieDrzwiVW"), GetPVarInt(playerid, "TworzenieDrzwiInt"), \
		GetPVarFloat(playerid, "TworzenieDrzwiX"), GetPVarFloat(playerid, "TworzenieDrzwiY"), GetPVarFloat(playerid, "TworzenieDrzwiZ"), Player[playerid][Door]);
		DInfo(playerid, str);
	}	
	if(dialogid == DIALOG_VEHICLES_LIST)
	{
	    if(response)
	    {
			new vehicleuid = strval(inputtext), string[126], title[126], query[64];
			new index = GetVehicleIndexByUID(vehicleuid);
			SetPVarInt(playerid, "vehicleuid", vehicleuid);
			
 			if(index > 0) 
			{
				format(title, sizeof(title), "{4876FF}Twoje pojazdy{a9c4e4} � %s (UID: %d)", VehiclesName[Vehicle[index][vModel] - 400], vehicleuid);
				format(string, sizeof(string), "Unspawnuj\nInformacje\nNamierz\n");
			}	
 			else
			{
			    format(query, sizeof(query), "SELECT `model` FROM `"prefix"vehicles` WHERE `id` = '%d'", vehicleuid);
				mysql_query(query);
				mysql_store_result();
				mysql_fetch_row_format(query);	
				new model = strval(query);
				mysql_free_result();

			    SetPVarInt(playerid, "vehmodel", model);
				format(title, sizeof(title), "{4876FF}Twoje pojazdy{a9c4e4} � %s (UID: %d)", VehiclesName[model - 400], vehicleuid);
				format(string, sizeof(string), "Spawnuj\n");
			}

 			format(string, sizeof(string), "%s---------------\nUsun(bezpowrotnie)\n", string);
 			if(GetPVarInt(playerid, "vehicleuid") > 0) ShowPlayerDialog(playerid, DIALOG_VEHICLES_LIST_2, DIALOG_STYLE_LIST, title, string, "Wybierz", "Zamknij");
	    }
	    return 1;
	}
	if(dialogid == DIALOG_VEHICLES_LIST_2)
	{
		new vehicleuid = GetPVarInt(playerid, "vehicleuid");
		new index = GetVehicleIndexByUID(vehicleuid);
		//new model = GetPVarInt(playerid, "vehmodel");
	    new string[128], title[100], command[15];
	    if(response)
	    {
	        switch(listitem)
	        {
	            case 0:
				{
				    if(!(Vehicle[index][vFlags] & FLAG_SPAWNED))
				    {
            			format(command, sizeof(command), "spawn %d", vehicleuid);
						cmd_v(playerid, command);
					}
				    else
				    {
				        format(command, sizeof(command), "unspawn %d", vehicleuid);
						cmd_v(playerid, command);
				    }
				}
				case 1: // Info
				{
					if(!(Vehicle[index][vFlags] & FLAG_SPAWNED)) return 1;
 					format(title, sizeof(title), "{4876FF}Twoje pojazdy{a9c4e4} � Informacje o %s (UID: %d)", VehiclesName[Vehicle[index][vModel] - 400], vehicleuid);
				    format(string, sizeof(string), "UID:\t\t%d\nModel:\t\t%d\n", vehicleuid, Vehicle[index][vModel]);
				    if(Vehicle[index][vHealth] > 260) format(string, sizeof(string), "%sHP:\t\t%.2f\n", string, Vehicle[index][vHealth]);
				    else format(string, sizeof(string), "%sHP:\t\t%.2f(Zniszczony)\n", string, Vehicle[index][vHealth]);
				    format(string, sizeof(string), "%sPrzebieg:\t%.3fkm\nZamkni�ty:\t%s\nKolor 1:\t\t%d\nKolor 2:\t\t%d\nRejestracja:\t%s\n", string, Vehicle[index][vDistance]/1000, \
													YesOrNo(Vehicle[index][vLocked]), Vehicle[index][vColor1], Vehicle[index][vColor2], Vehicle[index][vNrPlate]);
					ShowPlayerDialog(playerid, NONE, DIALOG_STYLE_LIST, title, string, "Zamknij", "");
				}
				case 2: // Namierz || Usu�
				{
					if(Vehicle[index][vFlags] & FLAG_SPAWNED)
				    {
						format(command, sizeof(command), "namierz %d", vehicleuid);
						cmd_v(playerid, command);
					}
					else print("pojazd skasowany");
				}
	        }
			return 1;
	    }
	}
	if(dialogid == DIALOG_DOORS)
	{
		if(!response) return 1;
		new id = Player[playerid][Door];
		switch(listitem)
		{
			case 0: // Zmiana nazwy systemowej
			{
				ShowPlayerDialog(playerid, DIALOG_DOORS_NAME, DIALOG_STYLE_INPUT, "{4876FF}Drzwi{a9c4e4} � Edycja", "{FFFFFF}Wpisz poni�ej now� nazw� systemow� drzwi", "Ok", "Anuluj");
			}
			case 1: // Nazwa wy�wietlana
			{
				ShowPlayerDialog(playerid, DIALOG_DOORS_TEXT, DIALOG_STYLE_INPUT, "{4876FF}Drzwi{a9c4e4} � Edycja", "{FFFFFF}Wpisz poni�ej now� nazw� wy�wietlan� drzwi", "Ok", "Anuluj");
			}
			case 2: //Informacje
			{
				new str[128];
				format(str, 128, "Nazwa drzwi: \t%s\nW�a�ciciel: \t%d\nKoszt wej�cia: \t%d$", DoorCache[id][dName], DoorCache[id][dOwner], DoorCache[id][dFee]);
				BSRPDialog(playerid, DUMMY, DIALOG_STYLE_LIST, "Drzwi", "Edycja", str, "Ok", "");
			}
			case 3: //przypisywanie
			{
				BSRPDialog(playerid, DIALOG_DOORS_GROUP, DIALOG_STYLE_LIST, "Drzwi", "Edycja", "Gracz\nGrupa", "Ok", "Anuluj");
			}
			case 4: //, 5, 6
			{
				if(DoorCache[id][dBuy] == 0) DoorCache[id][dBuy] = 1;
				else DoorCache[id][dBuy] = 0;
				DInfo(playerid, "W��czy�e� samoobs�ug�.");
				SaveDoors(id);
			}
			case 5:
			{
				if(DoorCache[id][dDrive] == 0) DoorCache[id][dDrive] = 1;
				else DoorCache[id][dDrive] = 0;
				DInfo(playerid, "W��czy�e� przejazd.");
				SaveDoors(id);
			}
			case 6:
			{
				new str[512], query[256], result[256], pid, name;
				format(query, 256, "SELECT id, name FROM bsrp_items WHERE ownertype=2 AND owner='%d'", id);
				mysql_query(query);
				mysql_store_result();
				while(mysql_fetch_row_format(result))
				{
					sscanf(result,  "p<|>ds[36]", pid, name);	
					
					format(str, sizeof(str), "%s\n%d\t%s", str, pid, name);
				}
				mysql_free_result();
				BSRPDialog(playerid, 666, DIALOG_STYLE_LIST, "Drzwi", "Schowek", str, "Wyci�gnij", "Anuluj");
			}			
			case 7: //Koszt wej�cia
			{
				BSRPDialog(playerid, DIALOG_DOORS_FEE, DIALOG_STYLE_INPUT, "Drzwi", "Edycja", "Wpisz poni�ej nowy koszt wej�cia dla drzwi.", "Ok", "Anuluj");
			}
			case 8: //Ustawianie w�a�ciciela
			{
				BSRPDialog(playerid, DIALOG_DOORS_OWNER, DIALOG_STYLE_INPUT, "Drzwi", "Edycja", "Wpisz poni�ej UID nowego w�a�ciciela drzwi.", "Ok", "Anuluj");
			}
			case 9: // 10, 11
			{
				if(DoorCache[id][dFreeze] == 0)
				{
					DoorCache[id][dFreeze] = 1;
					DInfo(playerid, "W��czy�e� zamra�anie po wej�ciu.");
				}
				else{
					DoorCache[id][dFreeze] = 0;
					DInfo(playerid, "Wy��czy�e� zamra�anie po wej�ciu.");
				}
				SaveDoors(id);
				
			}
			case 10:
			{
				BSRPDialog(playerid, 665, DIALOG_STYLE_INPUT, "Drzwi", "Kolor tekstu", "Wpisz poni�ej nowy kolor tekstu w formacie HEX. Przyk�adowo 0xFF0000AA.\nAby utworzy� taki kolor, wpisujemy 0x[RGB]AA-FF. Litery AA-FF oznaczaj� procentow�\nprze�roczysto�� tekstu.", "Ustaw", "Anuluj");
			}
			case 11:
			{
				BSRPDialog(playerid, 664, DIALOG_STYLE_INPUT, "Drzwi", "Zmiana tekstu", "Wpisz poni�ej now� zawarto�� tekstu na drzwiach.\nAby go wy��czy� wpisz poni�ej znak _ .", "Ustaw", "Anuluj");
			}
		}
		return 1;
	}
	if(dialogid == 665)
	{
		if(!response) return 1;
		new id = Player[playerid][Door];	
		format(DoorCache[id][dColor], 10, "%s", inputtext);
		Update3DTextLabelText(DoorCache[id][dDoorText], DoorCache[id][dColor], DoorCache[id][dOnDoor]);
		SaveDoors(id);
	}
	if(dialogid == 664)
	{
		if(!response) return 1;
		new id = Player[playerid][Door];	
		format(DoorCache[id][dOnDoor], 64, "%s", inputtext);
		Update3DTextLabelText(DoorCache[id][dDoorText], DoorCache[id][dColor], DoorCache[id][dOnDoor]);
		SaveDoors(id);
	}	
	if(dialogid == DIALOG_DOORS_NAME)
	{
		if(!response) return 1;
		new id = Player[playerid][Door];
		format(DoorCache[id][dName], 32, "%s", inputtext);
		SaveDoors(id);
	}
	if(dialogid == DIALOG_DOORS_TEXT)
	{
		if(!response) return 1;
		new id = Player[playerid][Door];
		format(DoorCache[id][dText], 64, "%s", inputtext);
		SaveDoors(id);
	}
	if(dialogid == DIALOG_DOORS_FEE)
	{
		if(!response) return 1;
		new id = Player[playerid][Door];
		DoorCache[id][dFee] = strval(inputtext);
		SaveDoors(id);
	}
	if(dialogid == DIALOG_DOORS_OWNER)
	{
		if(!response) return 1;
		new id = Player[playerid][Door];
		DoorCache[id][dOwner] = strval(inputtext);
		SaveDoors(id);
	}
	if(dialogid == DIALOG_ITEM_LIST)
	{
		if(!response) return 1;
		SetPVarInt(playerid, "ItemID", strval(inputtext));
		new options[64];
		format(options, 64, "Opcje[%d]", GetPVarInt(playerid, "ItemID"));
		if(GetPVarInt(playerid, "ItemID") > 0) BSRPDialog(playerid, DIALOG_ITEM_OPTIONS, DIALOG_STYLE_LIST, "Przedmioty", options, "U�yj\nOd��\nRozdziel\nPo��cz\nOferuj", "Wybierz", "Anuluj");
		return 1;
	}
	if(dialogid == DIALOG_ITEM_OPTIONS)
	{
		if(!response) return 1;
		switch(listitem)
		{
			case 0:
			{
				UseItem(playerid, GetPVarInt(playerid, "ItemID"));
				return 1;
			}
			case 1:
			{
				DropItem(playerid, GetPVarInt(playerid, "ItemID"));
				return 1;
			}
		}
	}
	if(dialogid == DIALOG_ITEM_PICKUP)
	{
		if(!response) return 1;
		TakeItem(playerid, strval(inputtext));
		return 1;
	}
	if( dialogid == DIALOG_TELEPORT )
	{
	    if( !response )
		{
 			new big_str[ 2048 ];

			for( new i = 1; i < sizeof( Interiory2 ); i++ )
	    		format( big_str, sizeof( big_str ), "%s\n%d\t\t%s", big_str, i, Interiory2[ i ][ intNazwa ] );
	    	ShowPlayerDialog(playerid, DIALOG_TELEPORT2, DIALOG_STYLE_LIST, "Interiory", big_str, "Wybierz", "Anuluj");
			return 1;
		}
		listitem++;
		SetPlayerInterior( playerid, Interiory[ listitem ][ intUniverse ] );
		SetPlayerPos( playerid, Interiory[ listitem ][ intPozX ], Interiory[ listitem ][ intPozY ],
		Interiory[ listitem ][ intPozZ ]);
	}
	if( dialogid == DIALOG_TELEPORT2 )
	{
	    if( !response ) return 1;
		listitem++;
		SetPlayerInterior( playerid, Interiory2[ listitem ][ intUniverse ] );
		SetPlayerPos( playerid, Interiory2[ listitem ][ intPozX ], Interiory2[ listitem ][ intPozY ],
		Interiory2[ listitem ][ intPozZ ]);
	}
	return 1;
}

public OnPlayerClickPlayer(playerid, clickedplayerid, source)
{
	return 1;
}

CallBack:MinTimer()
{
	foreach(new i : Player)
	{
		Player[i][Minutes] += 1;
		if(Player[i][Minutes] >= 60)
		{
			Player[i][Hours] += 1;
			Player[i][Minutes] = 0;
		}
	}
	return 1;
}

CallBack:MainTimer()
{
	new Float:vpos[3], Float:VehicleHealth, Float:PlayerHP, InPlayerVehicles[MAX_VEHICLES];
	
	foreach(new i : Player)
	{
		GetPlayerHealth(i, PlayerHP);
		if(PlayerHP < Player[i][HP])
		{
			TextDrawShowForPlayer(i, RedRadar);
			Player[i][HP] = PlayerHP;
			SetTimerEx("HideRadar", 1500, false, "d", i);
		}
		
		if(doortimer[i])
		{
			doortimer[i] --;
			    
			if(doortimer[i] <= 0)
			{
			    PlayerTextDrawHide(i, drzwitd);
			}
		}
		
		if(GetPlayerVehicleSeat(i)) continue;
		InPlayerVehicles[GetPlayerVehicleID(i)] = i;
	}
	
	if(PunishTime > 0)
	{
		PunishTime--;
		printf("%d", PunishTime);
	}
	
	if(PunishTime <= 0)
	{
		PunishTime = 0;
		TextDrawHideForAll(Text:TextDrawPunishTitle);
		TextDrawHideForAll(Text:TextDrawPunishText);
	}	
	
	foreach(new veh : Vehicle)
	{
		if(Vehicle[veh][vEngine])
		{
			GetVehiclePos(veh, vpos[0], vpos[1], vpos[2]);
			Vehicle[veh][vDistance] += floatsqroot(floatpower(vpos[0] - Vehicle[veh][vXx], 2) + floatpower(vpos[1] - Vehicle[veh][vYy], 2) + floatpower(vpos[2] - Vehicle[veh][vZz], 2));
			Vehicle[veh][vXx] = vpos[0];
			Vehicle[veh][vYy] = vpos[1];
			Vehicle[veh][vZz] = vpos[2];
		}
			
		if(InPlayerVehicles[veh] != -1)
		{
			GetVehicleHealth(veh, VehicleHealth);
			if(VehicleHealth < Vehicle[veh][vHealth])
			{
				SetVehicleHealth(veh, VehicleHealth);
				Vehicle[veh][vHealth] = VehicleHealth;
				TextDrawShowForPlayer(InPlayerVehicles[veh], RedRadar);
				SetTimerEx("HideRadar", 2000, false, "d", InPlayerVehicles[veh]);
				if(VehicleHealth < 260.0)
				{
					SetVehicleHealth(veh, 270.0);
					SetVehicleParamsEx(veh, Vehicle[veh][vEngine] = 0, VEHICLE_PARAMS_OFF, VEHICLE_PARAMS_OFF, Vehicle[veh][vLocked], VEHICLE_PARAMS_OFF, VEHICLE_PARAMS_OFF, -1);
					Vehicle[veh][vFlags] |= FLAG_DESTROYED;
				}
			}
		}
	}	
}
/*GetPlayerHealth(i, PlayerHP);
		if(PlayerHP < Player[i][HP])
		{
			TextDrawShowForPlayer(i, RedRadar);
			SetTimerEx("HideRadar", 2000, false, "d", i);
		}*/	
CallBack:KickPlayer(playerid) return Kick(playerid);

CallBack:HideRadar(playerid) return TextDrawHideForPlayer(playerid, RedRadar);


CallBack:LoadSettings()
{
	new query[256];
    mysql_query("SELECT `spawnx`, `spawny`, `spawnz`, `spawna`, `spawnvw`, `spawnint` FROM `"prefix"settings`");
	mysql_store_result();
	while(mysql_fetch_row_format(query, "|"))
	{
		sscanf(query, "p<|>ffffdd",
		Settings[Spawn][0],
	 	Settings[Spawn][1],
	 	Settings[Spawn][2],
	 	Settings[Spawn][3],
	 	Settings[SpawnVw],
	 	Settings[SpawnInt]);
	}
	mysql_free_result();
	print("Ustawienia zosta�y wczytane.");
}

CallBack:OnPlayerLogin(playerid, uid)
{
	new query[512], string[MAX_PLAYER_NAME];
	format(query, sizeof(query), "SELECT * FROM `"prefix"user_data` WHERE `id` = '%d'", uid);
    mysql_query(query);
    mysql_store_result();
    	
	Player[playerid][Name] = 0;
	
	
	if(mysql_num_rows() != 0)
	{
  		mysql_fetch_row_format(query, "|");
		sscanf(query, "p<|>ds[20]ddddddfdd",
		    Player[playerid][GUID],
			Player[playerid][Name],
			Player[playerid][Admin],
			Player[playerid][Cash],
			Player[playerid][Bank],
			Player[playerid][Hours],
			Player[playerid][Minutes],
			Player[playerid][Skin],
			Player[playerid][HP],
			Player[playerid][DutyGroup],
			Player[playerid][AdminJail]);
			
		printf("hp gracza: %f", Player[playerid][HP]);
		Player[playerid][UID] = uid;
		ResetPlayerMoney(playerid);
		Player[playerid][Logged] = 1;
		Player[playerid][CallingTo] = INVALID_PLAYER_ID;
		Player[playerid][PhoneNumber] = 0;
		
		format(Player[playerid][NameSpace], MAX_PLAYER_NAME, Player[playerid][Name]);
		UnderscoreToSpace(Player[playerid][NameSpace]);
		
		format(query, sizeof(query), "{0099ff}Witaj, {FFFFFF}%s {0099ff}na postaci: {FFFFFF}%s {999999}(UID %d, ID %d, GUID %d).{0099ff} Mi�ej gry!", Player[playerid][GlobalName], Player[playerid][NameSpace], Player[playerid][UID], playerid, Player[playerid][GUID]);
		SendClientMessage(playerid, COLOR_WHITE, query);
		
		SetPlayerName(playerid, Player[playerid][Name]);
		SetPlayerHealth(playerid, Player[playerid][HP]);
		
		format(string, sizeof(string), "%s (%d)", Player[playerid][Name], playerid);
		Player[playerid][Nick] = Create3DTextLabel(string, COLOR_GRAD1, 30.0, 40.0, 50.0, 14.0, 1);
		Attach3DTextLabelToPlayer(Player[playerid][Nick], playerid, 0.0, 0.0, 0.17);
		GivePlayerMoney(playerid, Player[playerid][Cash]);
		LoadGroupForPlayer(playerid);
		printf("%s(id: %d) zalogowa� si� pomy�lnie.", Player[playerid][Name], playerid);
		TextDrawSetString(Text:TextDrawSanNews, "LSNews ~>~ Aktualnie nic nie jest nadawane w radio.");
		TextDrawShowForPlayer(playerid, Text:TextDrawSanNews);
		SpawnPlayer(playerid);
	}	
	return 1;
}

CallBack:OnPlayerSave(playerid)
{
    if(IsPlayerConnected(playerid))
	{
		new query[256];

	    format(query, sizeof(query), "UPDATE `"prefix"user_data` SET `admin` = '%d', `cash` = '%d',\
		`bank`='%d', `hours`='%d', `minutes`='%d', `skin` = '%d', `hp` = '%.1f', `dutygroup`='%d',\
		`adminjail`='%d'	WHERE `id` = '%d'",
	    Player[playerid][Admin],	Player[playerid][Cash], Player[playerid][Bank],
		Player[playerid][Hours], Player[playerid][Minutes], Player[playerid][Skin],
		Player[playerid][HP], Player[playerid][DutyGroup], Player[playerid][AdminJail],
		Player[playerid][UID]);
		mysql_query(query);
    }
	return 1;
}


CMD:stats(playerid, cmdtext[])
{
	new str[512], grupasluzba[64];
	new slot = Player[playerid][DutyGroup];
	if(slot > 0)
	{
		format(grupasluzba, 64, "%s", Group[playerid][slot][gName]);
	}
	else
	{
		format(grupasluzba, 64, "Brak slu�by");
	}
	format(str, sizeof(str), "{a9c4e4}Opcja\tWarto��{C2C2C2}\nNick postaci:\t%s\nGot�wka:\t$%d\n\
	Bank:\t$%d\nCzas w grze:\t%dh %dmin\nS�u�ba:\t%s",
	Player[playerid][Name], Player[playerid][Cash], Player[playerid][Bank], Player[playerid][Hours],
	Player[playerid][Minutes], grupasluzba);
	BSRPDialog(playerid, DUMMY, DIALOG_STYLE_LIST, "Posta�", "Statystyki", str, "Ok", "");
	return 1;
}

CMD:tel(playerid, params[])
{
 	if(!Player[playerid][PhoneNumber])
  	{
   		ShowPlayerDialog(playerid, DUMMY, DIALOG_STYLE_MSGBOX, "Wyst�pi� b��d", "Nie posiadasz �adnego telefonu w u�yciu.\nSkorzystaj z komendy /p, by w��czy� telefon.", "Okej", "");
   		return 1;
   	}
	new number, string[256];
	if(sscanf(params, "d", number))
	{
		SendClientMessage(playerid, COLOR_GRAD1, "*/tel(efon) [numer telefonu]");
	}
	if(Player[playerid][PhoneNumber] == number)
	{
	    SendClientMessage(playerid, COLOR_DO, "* S�ycha� sygna� zaj�to�ci... *");
	    return 1;
	}
	if(Player[playerid][CallingTo] != INVALID_PLAYER_ID)
	{
	    ShowPlayerDialog(playerid, DUMMY, DIALOG_STYLE_MSGBOX, "Wyst�pi� b��d", "Aktualnie prowadzisz ju� jak�� rozmow� telefoniczn�.\nU�yj komendy /zakoncz, aby zako�czy� obecn� rozmow�.", "Okej", "");
	    return 1;
	}
	foreachEx(i, MAX_PLAYERS)
	{
	    if(Player[i][Logged] && Spawned[i])
	    {
	        if(Player[i][PhoneNumber] == number)
	        {
	            if(Player[i][CallingTo] != INVALID_PLAYER_ID)
	            {
	                SendClientMessage(playerid, COLOR_DO, "* S�ycha� sygna� zaj�to�ci... *");
	                return 1;
	            }
         		format(string, sizeof(string), "~y~~h~Polaczenie~n~~y~~h~wychodzace do:~n~~n~[%d]", number);
				SendClientMessage(playerid, COLOR_GRAD1, string);
	        
	            format(string, sizeof(string), "~y~~h~Polaczenie~n~~y~~h~przychodzace od:~n~~n~[%d]", Player[playerid][PhoneNumber]);
				SendClientMessage(playerid, COLOR_GRAD1, string);
				//UpdatePlayerPhoneMenu(i, string);
				
				
				Player[playerid][CallingTo] = i;
				SetPlayerSpecialAction(playerid, SPECIAL_ACTION_USECELLPHONE);
			 	SetPlayerAttachedObject(playerid, SLOT_PHONE, 330, 6);
				
				SendClientMessage(playerid, COLOR_DO, "* S�ycha� sygna� pr�by po��czenia si�... *");
				SendClientMessage(i, COLOR_YELLOW, "Tw�j telefon dzwoni, u�yj /odbierz, by odebra� rozmow�.");
				return 1;
	        }
	    }
	}
	SendClientMessage(playerid, COLOR_DO, "* Nie mo�na po��czy� si� z wybranym numerem...");
	return 1;
}

CMD:odbierz(playerid, params[])
{
	new string[128];
	if(Player[playerid][CallingTo] != INVALID_PLAYER_ID)
	{
	    ShowPlayerDialog(playerid, DUMMY, DIALOG_STYLE_MSGBOX, "Wyst�pi� b��d", "Aktualnie rozmawiasz ju� z kim� przez telefon.", "Okej", "");
	    return 1;
	}
	foreachEx(i, MAX_PLAYERS)
	{
	    if(Player[i][Logged] && Spawned[i])
	    {
	        if(Player[i][CallingTo] == playerid)
	        {
	            Player[playerid][CallingTo] = i;
	            
	            format(string, sizeof(string), "~y~~h~Trwa rozmowa~n~~y~~h~z numerem:~n~~n~[%d]", Player[i][PhoneNumber]);
	            SendClientMessage(playerid, COLOR_WHITE, string);
	            
	            format(string, sizeof(string), "~y~~h~Trwa rozmowa~n~~y~~h~z numerem:~n~~n~[%d]", Player[playerid][PhoneNumber]);
	            SendClientMessage(playerid, COLOR_WHITE, string);
	            
	            SetPlayerSpecialAction(playerid, SPECIAL_ACTION_USECELLPHONE);
             	SetPlayerAttachedObject(playerid, SLOT_PHONE, 330, 6);
	            
	            SendClientMessage(playerid, COLOR_YELLOW, "Odebrano telefon. Aby zako�czy� rozmow� u�yj komendy /zakoncz.");
                SendClientMessage(i, COLOR_YELLOW, "Odebrano telefon. Aby zako�czy� rozmow� u�yj komendy /zakoncz.");
				return 1;
	        }
	    }
	}
	ShowPlayerDialog(playerid, DUMMY, DIALOG_STYLE_MSGBOX, "Wyst�pi� b��d", "Nikt w danej chwili nie pr�buje si� z Tob� po��czy�.", "Okej", "");
	return 1;
}

CMD:zakoncz(playerid, params[])
{
	if(Player[playerid][CallingTo] == INVALID_PLAYER_ID)
	{
	    ShowPlayerDialog(playerid, DUMMY, DIALOG_STYLE_MSGBOX, "Wyst�pi� b��d", "Nie prowadzisz aktualnie �adnej rozmowy telefonicznej.", "Okej", "");
	    return 1;
	}
	foreachEx(i, MAX_PLAYERS)
	{
	    if(Player[i][Logged] && Spawned[i])
	    {
	        if(Player[i][CallingTo] == playerid)
	        {
	            Player[i][CallingTo] = INVALID_PLAYER_ID;
	            
   				SetPlayerSpecialAction(playerid, SPECIAL_ACTION_STOPUSECELLPHONE);
   				RemovePlayerAttachedObject(playerid, SLOT_PHONE);
   				
				SetPlayerSpecialAction(i, SPECIAL_ACTION_STOPUSECELLPHONE);
				RemovePlayerAttachedObject(i, SLOT_PHONE);
	            
	            if(Player[playerid][CallingTo] == i)
	            {
	                Player[playerid][CallingTo] = INVALID_PLAYER_ID;
	                
	            	SendClientMessage(playerid, COLOR_YELLOW, "Rozmowa zosta�a zako�czona.");
	            	SendClientMessage(i, COLOR_YELLOW, "Rozmowa zosta�a zako�czona.");
				}
				else
				{
				    SendClientMessage(playerid, COLOR_YELLOW, "Po��czenie zosta�o przerwane.");
                    SendClientMessage(i, COLOR_YELLOW, "Po��czenie zosta�o przerwane.");
				}
				return 1;
	        }
	    }
	}
	return 1;
}

CMD:sms(playerid, params[])
{
	if(!Player[playerid][PhoneNumber])
  	{
   		ShowPlayerDialog(playerid, DUMMY, DIALOG_STYLE_MSGBOX, "Wyst�pi� b��d", "Nie posiadasz �adnego telefonu w u�yciu.\nSkorzystaj z komendy /u, by w��czy� telefon.", "Okej", "");
   		return 1;
   	}
	new number, text[128], string[256];
	if(sscanf(params, "ds[128]", number, text))
	{
	    SendClientMessage(playerid, COLOR_GRAD1, "/sms [Nr. tel] [Tre��]");
	    return 1;
	}
	foreachEx(i, MAX_PLAYERS)
	{
	    if(Player[i][Logged] && Spawned[i])
	    {
	        if(Player[i][PhoneNumber] == number)
	        {
	            format(string, sizeof(string), "[SMS] %d: %s", Player[playerid][PhoneNumber], text);
	            SendClientMessage(i, COLOR_YELLOW, string);
	            
	            format(string, sizeof(string), "Wys�ano � [SMS] %d: %s", number, text);
	            SendClientMessage(playerid, COLOR_YELLOW, string);
	            
	            format(string, sizeof(string), "%s wysy�a SMS-a.", Player[i][NameSpace]);
	            SendClientMessageEx(10.0, playerid, string, COLOR_PURPLE, COLOR_PURPLE, COLOR_PURPLE, COLOR_PURPLE, COLOR_PURPLE);
	            
				format(string, sizeof(string), "* %s otrzyma� SMS-a.", Player[i][NameSpace]);
	            /*if(PlayerCache[i][pSex] == 1) 		format(string, sizeof(string), "* %s otrzyma� SMS-a.", PlayerName(i));
	            else if(PlayerCache[i][pSex] == 2) 	format(string, sizeof(string), "* %s otrzyma�a SMS-a.", PlayerName(i));*/
	            
	            SendClientMessageEx(10.0, i, string, COLOR_PURPLE, COLOR_PURPLE, COLOR_PURPLE, COLOR_PURPLE, COLOR_PURPLE);
				return 1;
			}
	    }
	}
	SendClientMessage(playerid, COLOR_DO, "* Wiadomo�� SMS nie zosta�a dostarczona...");
	return 1;
}

CMD:z(playerid, params[]) return cmd_zakoncz(playerid, params);
CMD:od(playerid, params[]) return cmd_odbierz(playerid, params);
CMD:telefon(playerid, params[]) return cmd_tel(playerid, params);
CMD:call(playerid, params[]) return cmd_tel(playerid, params);