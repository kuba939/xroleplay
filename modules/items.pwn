/*Braki:
- Komenda /p
- Tworzenie przedmiot�w
- U�ywanie przedmiot�w*/

#define	ITEM_GUN		1
#define ITEM_FOOD		2
#define ITEM_CIGGY		3
#define ITEM_PHONE		4
#define ITEM_FLASHLIGHT	5

#define TYPE_COUNT          40 // Ta sama liczba co w ostatnim itemie


#define MAX_ITEMS	10000

#define DIALOG_ITEM_LIST 	1666
#define DIALOG_ITEM_OPTIONS	1667
#define DIALOG_ITEM_PICKUP	1668

//W�a�ciciele
#define OWNER_NONE 		0 	//Pod�oga
#define OWNER_PLAYER	1 	//Gracz
#define OWNER_VEHICLE	2 	//Pojazd
#define OWNER_DOOR		3 	//Schowek w drzwiach

new Iterator:Items<MAX_ITEMS>;


enum iItems
{
	iID,
	iName[64],
	iType,
	iVal1,
	iVal2,
	iObject,
	iOwner,
	iOwnerType,
	Float:iPosX,
	Float:iPosY,
	Float:iPosZ,
	Float:iRotX,
	Float:iRotY,
	Float:iRotZ,
	iVW,
	iInt,
	iUsed
}
new ItemCache[MAX_ITEMS][iItems];

enum sItemTypeInfo
{
	iTypeName[32],
	Float:iTypeWeight,
	
	iTypeObjModel,
	
	Float:iTypeObjRotX,
	Float:iTypeObjRotY
}
new ItemTypeInfo[TYPE_COUNT + 1][sItemTypeInfo] =
{
	/* nazwa | waga | model | rotx | roty   */
	{"Nieokre�lony", 	0.0, 328, 90.0, 95.0},	
	{"Bro�", 			0.0, 328, 90.0, 95.0},
	{"Jedzenie", 		0.0, 330, 0.0,	 0.0},
	{"Papierosy", 		0.0, 1485, 0.0,  0.0},	
	{"Telefon", 		0.0, 2769, 90.0,  0.0},
	{"Latarka",         0.0, 18641, 0.0, 0.0},	
	{"Zegarek", 		0.0, 2710, 0.0,  0.0},
	{"Kostka do gry", 	0.0, 328, 90.0, 95.0},
	{"Ubranie", 		0.0, 2843, 0.0,  0.0},
	{"Amunicja", 		0.0, 328, 90.0, 95.0},
	{"Kanister", 		0.0, 1650, 90.0, 0.0},
	{"Maska", 			0.0, 328, 90.0, 95.0},
	{"Paralizator", 	0.0, 328, 90.0, 95.0},
	{"Lakier", 			0.0, 328, 90.0, 95.0},
	{"Kajdanki", 		0.0, 328, 90.0, 95.0},
	{"Megafon", 		0.0, 328, 90.0, 95.0},
	{"Lina", 			0.0, 328, 90.0, 95.0},
	{"Pluskwa", 		0.0, 328, 90.0, 95.0},
	{"Notes",           0.0, 2894, 0.0,  0.0},
	{"Karteczka",       0.0, 328, 90.0, 95.0},
	{"Tuning",          0.0, 328, 90.0, 95.0},
	{"Ks. Czekowa", 	0.0, 2894, 0.0,  0.0},
	{"Czek",            0.0, 328, 90.0, 95.0},
	{"W�dka",           0.0, 18632, 90.0, 90.0},
	{"Torba",           0.0, 2663, 90.0,  0.0},
	{"Nap�j",           0.0, 328, 90.0, 95.0},
	{"Narkotyk",        0.0, 328, 90.0, 95.0},
	{"Joint", 			0.0, 1485, 0.0,  0.0},
	
	{"Bletka",          0.0, 328, 90.0, 95.0},
	{"Bongos",          0.0, 328, 90.0, 95.0},
	{"Lufka",           0.0, 328, 90.0, 95.0},
	
	{"Przyczepialny",   0.0, 328, 90.0, 95.0},
	{"Akcesoria",       0.0, 328, 90.0, 95.0},
	{"P�yta",           0.0, 1961, 90.0, 180.0},
	{"Odtwarzacz",      0.0, 328, 90.0, 95.0},
	{"Kluczyki",        0.0, 328, 90.0, 95.0},
	{"Kamizelka",       0.0, 328, 90.0, 95.0},
	{"R�kawiczki",      0.0, 328, 90.0, 95.0},
	{"Zw�oki",          0.0, 2060, 0.0, 0.0},
	{"Boombox",         0.0, 2103, 90.0, 95.0},
	{"Celownik laserowy",0.0, 18641, 0.0, 0.0}
};
	
new WeaponModel[47] = {0, 331, 333, 334, 335, 336, 337, 338, 339, 341, 321, 322, 323, 324, 325, 326, 342, 343, 344, 0, 0, 0, 346, 347, 348, 349, 350, 351, 352, 353, 355, 356, 372, 357, 358, 359, 360, 361, 362, 363, 364, 365, 366, 367, 368, 369, 371};
	
enum sItemObject {objItemUID, objID}
new ItemObject[MAX_ITEMS][sItemObject];

//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//STOCKI::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
stock GetFreeIndexItems()
{
	for(new i = 1; i < MAX_ITEMS; i++)
	{
		if(ItemCache[i][iID] == 0) return i;
	}
	return 0;
}	

stock GetItemIndexByUID(uid)
{
	foreach(new i : Items)
	{
		if(ItemCache[i][iID] == uid) return i;
	}
	return 0;
}


stock GetFreeItemObjectID()
{
	new object_id;
    for (new obj = 1; obj < MAX_ITEMS; obj++)
	{
	    if(!ItemObject[obj][objItemUID])
	    {
	        object_id = obj;
	        break;
	    }
	}
	return object_id;
}

stock GetItemObjectID(itemuid)
{
	new object_id;
 	for (new obj = 1; obj < MAX_ITEMS; obj++)
	{
	    if(ItemObject[obj][objItemUID] == itemuid)
	    {
	        object_id = obj;
	        break;
	    }
	}
	return object_id;
}

stock GetPlayerWeaponSlot(playerid, weaponid)
{
	new slotid, weapons[13][2];
	foreachEx(i, 13)
	{
		GetPlayerWeaponData(playerid, i, weapons[i][0], weapons[i][1]);
		if(weapons[i][0] == weaponid)
		{
			slotid = i;
		}
	}
	return slotid;
}

stock GetPlayerWeaponAmmo(playerid, weaponid)
{
	new ammo, weapons[13][2];
	foreachEx(i, 13)
	{
		GetPlayerWeaponData(playerid, i, weapons[i][0], weapons[i][1]);
		if(weapons[i][0] == weaponid)
		{
			ammo = weapons[i][1];
		}
	}
	return ammo;
}

stock GetXYInFrontOfPlayer(playerid, &Float:x, &Float:y, Float:distance) //by Y_Less
{
 	new Float:a;
 	GetPlayerPos(playerid, x, y, a);
 	GetPlayerFacingAngle(playerid, a);
 	
 	x += (distance * floatsin(-a, degrees));
 	y += (distance * floatcos(-a, degrees));
}


//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//CALLBACKI:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

CallBack:LoadItems()
{
	new result[512], uid = 1;
	mysql_query("SELECT * FROM `"prefix"items`");
	mysql_store_result();
	while(mysql_fetch_row_format(result))
	{
		sscanf(result,  "p<|>ds[64]ddddddffffffdd", 		
		ItemCache[uid][iID],
		ItemCache[uid][iName],
		ItemCache[uid][iType],
		ItemCache[uid][iVal1],
		ItemCache[uid][iVal2],
		ItemCache[uid][iObject],
		ItemCache[uid][iOwner],
		ItemCache[uid][iOwnerType],
		ItemCache[uid][iPosX],
		ItemCache[uid][iPosY],
		ItemCache[uid][iPosZ],
		ItemCache[uid][iRotX],
		ItemCache[uid][iRotY],
		ItemCache[uid][iRotZ],
		ItemCache[uid][iVW],
		ItemCache[uid][iInt],
		ItemCache[uid][iUsed]);	
		
		//if(DoorCache[uid][dPickupHide] == 0) 
		
		Iter_Add(Items, uid);
		
		printf("%s", ItemCache[uid][iName]);
		
		if(ItemCache[uid][iOwnerType] == OWNER_NONE)
		{
			new object_id = GetFreeItemObjectID();
			ItemObject[object_id][objItemUID] = ItemCache[uid][iID];
			if(ItemCache[uid][iType] == ITEM_GUN) // || ItemInfo[itemid][iType] == TYPE_INHIBITOR || ItemInfo[itemid][iType] == TYPE_PAINT
			{
				ItemObject[object_id][objID] = CreateStreamObject(WeaponModel[ItemCache[uid][iVal1]], ItemCache[uid][iPosX], ItemCache[uid][iPosY], ItemCache[uid][iPosZ] - 1.0, 80.0, 0.0, -ItemCache[uid][iRotZ], 80.0, 0, ItemCache[uid][iVW]);
			}
			else
			{
				ItemObject[object_id][objID] = CreateStreamObject(ItemTypeInfo[ItemCache[uid][iType]][iTypeObjModel], ItemCache[uid][iPosX], ItemCache[uid][iPosY], ItemCache[uid][iPosZ] - 1.0, ItemTypeInfo[ItemCache[uid][iType]][iTypeObjRotX], ItemTypeInfo[ItemCache[uid][iType]][iTypeObjRotY], -ItemCache[uid][iRotZ], 80.0, 0, ItemCache[uid][iVW]);
			}
		}
		uid++;
	}
	printf("##[ITEMS] Loaded count: %d", uid);
	mysql_free_result();
	return 1;
}

CallBack:CreateItem(playerid, name[], type, val1, val2)
{
	new item_uid, itemid;
	
	if(type == ITEM_PHONE) 		val1 = 100000 + random(899999);
	
	mysql_real_escape_string(name, name);
	new query[512];
	format(query, sizeof(query), "INSERT INTO `"prefix"items` (name, type, val1, val2, object, owner, ownertype) VALUES ('%s', '%d', '%d', '%d', '0', '%d', '%d')", name, type, val1, val2, Player[playerid][UID], OWNER_PLAYER);
	mysql_query(query);
	
	itemid = GetFreeIndexItems();
	item_uid = mysql_insert_id();
	
	Iter_Add(Items, itemid);
	
	ItemCache[itemid][iID] = item_uid;
	strmid(ItemCache[itemid][iName], name, 0, strlen(name), 32);
	
	ItemCache[itemid][iVal1] = val1;
	ItemCache[itemid][iVal2] = val2;
	
	ItemCache[itemid][iType] = type;
	
	ItemCache[itemid][iOwnerType] = OWNER_PLAYER;
	ItemCache[itemid][iOwner] = Player[playerid][UID];
		
	return item_uid;
}

CallBack:TakeItem(playerid, itemid)
{
	ApplyAnimation(playerid,"BOMBER","BOM_Plant",4.1,0,0,0,0,0,1);
	new query[256];
	format(query, sizeof(query), "UPDATE `"prefix"items` SET ownertype = '%d', owner = '%d' WHERE id = '%d' LIMIT 1", OWNER_PLAYER, Player[playerid][UID], ItemCache[itemid][iID]);
	mysql_query(query);	
	
	new object_id = GetItemObjectID(itemid);
	DestroyStreamObject(ItemObject[object_id][objID]);
	return 1;
}

CallBack:ListPlayerNearItems(playerid)
{
	new data[64], list_items[512];
	if(!IsPlayerInAnyVehicle(playerid))
	{
		new Float:PosX, Float:PosY, Float:PosZ, query[280];
		GetPlayerPos(playerid, PosX, PosY, PosZ);
		
		format(query, sizeof(query), "SELECT `id`, `name` FROM `"prefix"items` WHERE posx < %f + 2 AND posx > %f - 2 AND posy < %f + 2 AND posy > %f - 2 AND posz < %f + 2 AND posz > %f - 2 AND ownertype = '%d' AND vw = '%d'", 
		PosX, PosX, PosY, PosY, PosZ, PosZ, OWNER_NONE, GetPlayerVirtualWorld(playerid));
		mysql_query(query);
	}
	
	new	item_uid, item_name[64];

	mysql_store_result();
   	while(mysql_fetch_row_format(data, "|"))
   	{
     	sscanf(data, "p<|>ds[64]", item_uid, item_name);
     	format(list_items, sizeof(list_items), "%s\n%d\t%s", list_items, item_uid, item_name);
	}
  	mysql_free_result();
  	if(!strlen(list_items))
  	{
     	DInfo(playerid, "Nie znaleziono przedmiot�w w pobli�u.");
     	return 1;
	}
	ShowPlayerDialog(playerid, DIALOG_ITEM_PICKUP, DIALOG_STYLE_LIST, "Pobliskie przedmioty", list_items, "Podnie�", "Anuluj");
	return 1;
}


CallBack:DropItem(playerid, itemid)
{

	if(GetPlayerState(playerid) == PLAYER_STATE_ONFOOT)
	{
	    new Float:PosX, Float:PosY, Float:PosZ, Float:PosA, virtual_world = GetPlayerVirtualWorld(playerid);

		GetPlayerPos(playerid, PosX, PosY, PosZ);
		GetPlayerFacingAngle(playerid, PosA);


		ApplyAnimation(playerid,"BOMBER","BOM_Plant",4.1,0,0,0,0,0,1);
		new query[256];
		format(query, sizeof(query), "UPDATE `"prefix"items` SET posx = '%f', posy = '%f', posz = '%f', ownertype = '%d', vw = '%d' WHERE id = '%d' LIMIT 1", PosX, PosY, PosZ, OWNER_NONE, virtual_world, ItemCache[itemid][iID]);
		mysql_query(query);
		new object_id = GetFreeItemObjectID();
		ItemObject[object_id][objItemUID] = ItemCache[itemid][iID];
		if(ItemCache[itemid][iType] == ITEM_GUN) // || ItemInfo[itemid][iType] == TYPE_INHIBITOR || ItemInfo[itemid][iType] == TYPE_PAINT
		{
		    ItemObject[object_id][objID] = CreateStreamObject(WeaponModel[ItemCache[itemid][iVal1]], PosX, PosY, PosZ - 1.0, 80.0, 0.0, -PosA, 80.0, 0, virtual_world);
		}
		else
		{
            ItemObject[object_id][objID] = CreateStreamObject(ItemTypeInfo[ItemCache[itemid][iType]][iTypeObjModel], PosX, PosY, PosZ - 1.0, ItemTypeInfo[ItemCache[itemid][iType]][iTypeObjRotX], ItemTypeInfo[ItemCache[itemid][iType]][iTypeObjRotY], -PosA, 80.0, 0, virtual_world);
		}
		
	}
	return 1;
}

CallBack:SaveItem(uid)
{
	new sql[512];
	format(sql, 512, "UPDATE "prefix"items SET name='%s', type='%d', val1='%d', val2='%d', object='%d', owner='%d', ownertype='%d',\
	posx='%f', posy='%f', posz='%f', rotx='%f', roty='%f', rotz='%f', vw='%d', int='%d', using='%d' WHERE id='%d'", ItemCache[uid][iName],
	ItemCache[uid][iType], ItemCache[uid][iVal1], ItemCache[uid][iVal2], ItemCache[uid][iObject], ItemCache[uid][iOwner], ItemCache[uid][iOwnerType],
	ItemCache[uid][iPosX], ItemCache[uid][iPosY], ItemCache[uid][iPosZ], ItemCache[uid][iRotX], ItemCache[uid][iRotY], ItemCache[uid][iRotZ],
	ItemCache[uid][iVW], ItemCache[uid][iInt], ItemCache[uid][iUsed], uid);
	mysql_query(sql);
	print("LOLOLOL");
	return 1;
}


CallBack:UseItem(playerid, itemid)
{
	new query[128];
	if(ItemCache[itemid][iVal2] <= 0)
	{
		DInfo(playerid, "Przedmiot si� sko�czy�.");
		return 1;
	}
	if(ItemCache[itemid][iType] != ITEM_GUN && ItemCache[itemid][iType] != ITEM_PHONE && ItemCache[itemid][iType] != ITEM_FLASHLIGHT)
	{
		ItemCache[itemid][iVal2] -= 1;
		format(query, sizeof(query), "UPDATE `"prefix"items` SET val2=%d WHERE id = '%d' LIMIT 1", ItemCache[itemid][iVal2], ItemCache[itemid][iID]);
		mysql_query(query);	
	}
	if(ItemCache[itemid][iType] == ITEM_GUN)
	{
		if(!ItemCache[itemid][iUsed])
		{
			GivePlayerWeapon(playerid, ItemCache[itemid][iVal1], ItemCache[itemid][iVal2]);
			ItemCache[itemid][iUsed] = 1;
			//format(query, sizeof(query), "UPDATE `"prefix"items` SET val2=%d WHERE id = '%d' LIMIT 1", ItemCache[itemid][iVal2], ItemCache[itemid][iID]);
			Player[playerid][WeaponUID] = ItemCache[itemid][iID];
			Player[playerid][WeaponID] = ItemCache[itemid][iVal1];
		}
		else
		{
			ItemCache[itemid][iVal2] =  GetPlayerWeaponAmmo(playerid, ItemCache[itemid][iVal1]);
			ResetPlayerWeapons(playerid);
			ItemCache[itemid][iUsed] = 0;
			format(query, sizeof(query), "UPDATE `"prefix"items` SET val2=%d WHERE id = '%d' LIMIT 1", GetPlayerWeaponAmmo(playerid, ItemCache[itemid][iVal1]), ItemCache[itemid][iID]);
			Player[playerid][WeaponUID] = 0;
			Player[playerid][WeaponID] = 0;
		}
	}
	if(ItemCache[itemid][iType] == ITEM_FOOD)
	{
		new Float:hp;
		GetPlayerHealth(playerid, hp);
		SetPlayerHealth(playerid, hp + ItemCache[itemid][iVal1]);
		Player[playerid][HP] = hp + ItemCache[itemid][iVal1];
	}
	if(ItemCache[itemid][iType] == ITEM_PHONE)
	{
		if(!ItemCache[itemid][iUsed])
		{
			if(Player[playerid][PhoneNumber] != 0) return DInfo(playerid, "Mo�esz mie� w��czony tylko jeden telefon!");
			Player[playerid][PhoneNumber] = ItemCache[itemid][iVal1];
			ItemCache[itemid][iUsed] = 1;
			GameTextForPlayer(playerid, "~w~Telefon ~g~wlaczony", 4000, 6);
			format(query, sizeof(query), "UPDATE `"prefix"items` SET using = '1' WHERE id = '%d' LIMIT 1", ItemCache[itemid][iID]);
		}
		else
		{
			Player[playerid][PhoneNumber] = 0;
			ItemCache[itemid][iUsed] = 0;
			GameTextForPlayer(playerid, "~w~Telefon ~g~wylaczony", 4000, 6);
			format(query, sizeof(query), "UPDATE `"prefix"items` SET using = '0' WHERE id = '%d' LIMIT 1", ItemCache[itemid][iID]);		
		}
		mysql_query(query);
	}
	if(ItemCache[itemid][iType] == ITEM_CIGGY)
	{
		SetPlayerSpecialAction(playerid, SPECIAL_ACTION_SMOKE_CIGGY);
	}
	if(ItemCache[itemid][iType] == ITEM_FLASHLIGHT)
	{
		if(IsPlayerInAnyVehicle(playerid))
		{
		    ShowPlayerDialog(playerid, DUMMY, DIALOG_STYLE_MSGBOX, "Wyst�pi� b��d", "Nie mo�esz by� w poje�dzie.", "Okej", "");
		    return 1;
		}
		if(IsPlayerAttachedObjectSlotUsed(playerid, SLOT_OTHER_ITEM)) RemovePlayerAttachedObject(playerid, SLOT_OTHER_ITEM);
		if(ItemCache[itemid][iUsed])
		{
			ItemCache[itemid][iUsed] = 0;
		}
		else
		{
			SetPlayerAttachedObject(playerid, SLOT_OTHER_ITEM, 18641, 5, 0.1, 0.02, -0.05, 0, 0, 0, 1, 1, 1);
			ItemCache[itemid][iUsed] = 1;
		}
		format(query, sizeof(query), "UPDATE `"prefix"items` SET using = '%d' WHERE id = '%d' LIMIT 1", ItemCache[itemid][iUsed], ItemCache[itemid][iID]);
		mysql_query(query);
	}	
	return 1;
}

//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//KOMENDY:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

CMD:p(playerid, params[])
{
	new	typ[32], reszta[128], query[256], index;
	if(sscanf(params, "s[32]S()[128]", typ, reszta))
	{
		DInfo(playerid, "{FF0000}Wskaz�wka{FFFFFF}[/p]:\nlista, podnies, odloz");
		return 1;
	}
	else if(!strcmp(typ, "lista", true))
	{
		new name[64], val1, val2;
	    format(query, sizeof(query), "SELECT `id`, `name` FROM `"prefix"items` WHERE `owner` = '%d' AND `ownertype`='1' ORDER BY `id`", Player[playerid][UID]);
    	mysql_query(query);
    	mysql_store_result();

		new str[1024];
		format(str, sizeof(str), "{a9c4e4}Nazwa\t\tWarto��\t\tIlo��{ffffff}\n");
		
		if(mysql_num_rows() > 0)
    	{
    		while(mysql_fetch_row_format(query, "|"))
      		{
	    		sscanf(query, "p<|>ds[64]", index, name);
				val1 = ItemCache[index][iVal1];
				val2 = ItemCache[index][iVal2];
                if(ItemCache[index][iUsed])
                {
                    if(ItemCache[index][iVal2] <= 0) format(str, sizeof(str), "%s%d. %s{a7a7a7}(Sko�czony){000000}\t\t%d\t%d{ffffff}\n", str, index, name, val1, val2);
					else format(str, sizeof(str), "%s%d. {99ff66}%s{000000}\t\t%d\t%d{ffffff}\n", str, index, name, val1, val2);
				}
				else format(str, sizeof(str), "%s%d. %s{000000}\t%d\t%d{ffffff}\n", str, index, name, val1, val2);
				ShowPlayerDialog(playerid, DIALOG_ITEM_LIST, DIALOG_STYLE_LIST, "{4876FF}Przedmioty{a9c4e4} � Lista", str, "Wybierz", "Zamknij");
			}
		}
		else return GameTextForPlayer(playerid, "~r~Nie posiadasz zadnych przedmiotow!", 3000, 3);
		mysql_free_result();
		return 1;
	}
	else if(!strcmp(typ, "podnies", true))
	{
		ListPlayerNearItems(playerid);
	}
	return 1;
}

CMD:ap(playerid, params[])
{
	if(Player[playerid][Admin] < 3) return DInfo(playerid, "ERROR[403]: Forbidden\n{FF0000}Brak uprawnie� do komendy");
	new	typ[32], reszta[128], query[512];
	if(sscanf(params, "s[32]S()[128]", typ, reszta))
	{
		DInfo(playerid, "Wskaz�wka[/ap]:\nstworz, usun, typ, val1, val2, owner");
		return 1;
	}
	else if(!strcmp(typ, "owner", true))
	{
		new oid, otype, id;
		if(sscanf(reszta, "ddd", id, otype, oid))
		{
			DInfo(playerid, "Wskaz�wka[/ap -> owner]:\n[id przedmiotu] [typ w�a�ciciela] [id w�a�ciciela]");
		}
		else
		{
			format(query, sizeof(query), "UPDATE "prefix"items SET ownertype='%d', owner='%d' WHERE id='%d'", otype, oid, ItemCache[id][iID]);
			mysql_query(query);
			mysql_free_result();
			ItemCache[id][iOwner] = oid;
			ItemCache[id][iOwnerType] = otype;
		}
	}
	else if(!strcmp(typ, "stworz", true))
	{
		new type, val1, val2, nazwa[64];
		if(sscanf(reszta, "ddds[64]", type, val1, val2, nazwa))
		{
			SendClientMessage(playerid, COLOR_GRAD1, "/ap stworz [typ] [warto��] [warto��] [nazwa]");
		}
		else
		{
			CreateItem(playerid, nazwa, type, val1, val2);
			SendClientMessage(playerid, COLOR_GRAD1, "Utworzony przedmiot znajduje si� w ekwipunku.");
		}
	}
	return 1;
}