#define TYPE_KICK		1
#define TYPE_AJ			2
#define TYPE_WARN		3
#define TYPE_BLOCK		4
#define TYPE_RUN		5
#define TYPE_BAN		6
#define TYPE_RESPECT 	7

stock EscapePL(name[])
{
    for(new i = 0; name[i] != 0; i++)
    {
	    if(name[i] == '�') name[i] = 's';
	    else if(name[i] == '�') name[i] = 'e';
	    else if(name[i] == '�') name[i] = 'o';
	    else if(name[i] == '�') name[i] = 'a';
	    else if(name[i] == '�') name[i] = 'l';
	    else if(name[i] == '�') name[i] = 'z';
	    else if(name[i] == '�') name[i] = 'z';
	    else if(name[i] == '�') name[i] = 'c';
	    else if(name[i] == '�') name[i] = 'n';
	    else if(name[i] == '�') name[i] = 'S';
	    else if(name[i] == '�') name[i] = 'E';
	    else if(name[i] == '�') name[i] = 'O';
	    else if(name[i] == '�') name[i] = 'A';
	    else if(name[i] == '�') name[i] = 'L';
	    else if(name[i] == '�') name[i] = 'Z';
	    else if(name[i] == '�') name[i] = 'Z';
	    else if(name[i] == '�') name[i] = 'C';
	    else if(name[i] == '�') name[i] = 'N';
	    //else if(name[i] == ' ') name[i] = '_';
    }
}

CallBack:AddPunishLog(playerid, giveid, type, reason[])
{
	new query[256];
	mysql_real_escape_string(reason, reason);
	format(query, sizeof(query), "INSERT INTO `"prefix"punishes` (`char`, `give`, `type`, `reason`, `time`) VALUES ('%d', '%d', '%d', '%s', 'NOW()')", Player[playerid][UID], Player[giveid][UID], type, reason);
	mysql_query(query);
	return 1;
}

CallBack:GiveAdminJail(playerid, giveid, reason[], time)
{
	new string[128], string2[128], giver_name[24];
	if(giveid == -1) strmid(giver_name, "System", 0, 6, 24);
	else strmid(giver_name, Player[playerid][NameSpace], 0, strlen(Player[playerid][NameSpace]), 24);
	
	EscapePL(reason);
	
	AddPunishLog(playerid, giveid, TYPE_AJ, reason);
	
	/*TextDrawPunishTitle = TextDrawCreate(7.000000, 246.166656, "~r~AdminJail~w~ 25h 35min");
	TextDrawPunishText = TextDrawCreate(7.000000, 260.166687, "~y~Dla: ~w~Denzel Grands~n~~y~Od: ~w~John Kennedy~n~~y~Powod: ~w~Kosi slupy");	*/

    format(string, sizeof(string), "~r~AdminJail~w~ %dmin", time);
	TextDrawSetString(Text:TextDrawPunishTitle, string);
	TextDrawShowForAll(Text:TextDrawPunishTitle);
	
    format(string2, sizeof(string2), "~y~Dla: ~w~%s~n~~y~Od: ~w~%s~n~~y~Powod: ~w~%s", Player[playerid][NameSpace], Player[giveid][NameSpace], reason);
	TextDrawSetString(Text:TextDrawPunishText, string2);
	TextDrawShowForAll(Text:TextDrawPunishText);	

	SetPlayerPos(playerid, 154.1221, -1951.9156, 47.8750);
	SetPlayerInterior(playerid, 0);
	SetPlayerVirtualWorld(playerid, playerid + 1000);

	Player[playerid][AdminJail] = time;
	
	PunishTime = 15;
	
	return 1;
}

CallBack:GiveKick(playerid, giveid, reason[])
{
	new string2[128], giver_name[24];
	if(giveid == -1) strmid(giver_name, "System", 0, 6, 24);
	else strmid(giver_name, Player[playerid][NameSpace], 0, strlen(Player[playerid][NameSpace]), 24);
	
	EscapePL(reason);
	
	AddPunishLog(playerid, giveid, TYPE_KICK, reason);
	
	/*TextDrawPunishTitle = TextDrawCreate(7.000000, 246.166656, "~r~AdminJail~w~ 25h 35min");
	TextDrawPunishText = TextDrawCreate(7.000000, 260.166687, "~y~Dla: ~w~Denzel Grands~n~~y~Od: ~w~John Kennedy~n~~y~Powod: ~w~Kosi slupy");	*/

	TextDrawSetString(Text:TextDrawPunishTitle, "~r~Kick~w~");
	TextDrawShowForAll(Text:TextDrawPunishTitle);
	
    format(string2, sizeof(string2), "~y~Dla: ~w~%s~n~~y~Od: ~w~%s~n~~y~Powod: ~w~%s", Player[playerid][NameSpace], Player[giveid][NameSpace], reason);
	TextDrawSetString(Text:TextDrawPunishText, string2);
	TextDrawShowForAll(Text:TextDrawPunishText);	

	Kick(playerid);
	
	PunishTime = 15;
	
	return 1;
}

CMD:aj(playerid, params[])
{
	new id, time, reason[64];
	if(Player[playerid][Admin] < 1) return SendClientMessage(playerid, COLOR_GRAD1, "Nie masz uprawnie� do u�ywania tej komendy.");
	
	if(sscanf(params, "dds[64]", id, time, reason))
	{
		SendClientMessage(playerid, COLOR_GRAD1, "U�yj: /aj [id gracza] [czas(w minutach)] [pow�d]");
		return 1;
	}
	else
	{
		if(id == INVALID_PLAYER_ID) return SendClientMessage(playerid, COLOR_GRAD1, "Z�e id gracza.");
		GiveAdminJail(id, playerid, reason, time);
		DInfo(playerid, "Otrzyma�e� AdminJaila. Postaraj si� poprawi� swoje zachowanie\nponiewa� mog� ciebie czeka� ci�kie kary. W przypadku\npomy�ki, skontaktuj si� z administratorem i to wyja�nij,\n\
		bez zb�dnego spamu.");
	}
	return 1;
}

CMD:kick(playerid, params[])
{
	new id, reason[64];
	if(Player[playerid][Admin] < 1) return SendClientMessage(playerid, COLOR_GRAD1, "Nie masz uprawnie� do u�ywania tej komendy.");
	
	if(sscanf(params, "ds[64]", id, reason))
	{
		SendClientMessage(playerid, COLOR_GRAD1, "/kick [id gracza] [pow�d]");
		return 1;
	}
	else
	{
		if(id == INVALID_PLAYER_ID) return SendClientMessage(playerid, COLOR_GRAD1, "Z�e id gracza.");
		GiveKick(id, playerid, reason);
	}
	return 1;
}