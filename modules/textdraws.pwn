
stock CreateTextDraws()
{
	EngineVehicles = TextDrawCreate(499.000000, 105.000000, "Aby wlaczyc silnik~n~uzyj ~r~L.Ctrl + L.Alt");
	TextDrawBackgroundColor(EngineVehicles, 255);
	TextDrawFont(EngineVehicles, 1);
	TextDrawLetterSize(EngineVehicles, 0.350000, 1.100000);
	TextDrawColor(EngineVehicles, -1);
	TextDrawSetOutline(EngineVehicles, 0);
	TextDrawSetProportional(EngineVehicles, 1);
	TextDrawSetShadow(EngineVehicles, 1);
	TextDrawUseBox(EngineVehicles, 1);
	TextDrawBoxColor(EngineVehicles, 101);
	TextDrawTextSize(EngineVehicles, 606.000000, 0.000000);
	
	TextDrawSanNews = TextDrawCreate(2.000000, 438.000000, "LSNews ~>~ Aktualnie nic nie jest nadawane w radio.");
	TextDrawBackgroundColor(TextDrawSanNews, 255);
	TextDrawFont(TextDrawSanNews, 1);
	TextDrawLetterSize(TextDrawSanNews, 0.250000, 0.899999);
	TextDrawColor(TextDrawSanNews, -1);
	TextDrawSetOutline(TextDrawSanNews, 1);
	TextDrawSetProportional(TextDrawSanNews, 1);
	TextDrawUseBox(TextDrawSanNews, 1);
	TextDrawBoxColor(TextDrawSanNews, 68);
	TextDrawTextSize(TextDrawSanNews, 640.000000, 600.000000);
	
	TextDrawPunishTitle = TextDrawCreate(7.000000, 246.166656, "~r~AdminJail~w~ 25h 35min");
	TextDrawLetterSize(TextDrawPunishTitle, 0.278499, 1.045832);
	TextDrawTextSize(TextDrawPunishTitle, 144.500000, -14.000002);
	TextDrawAlignment(TextDrawPunishTitle, 1);
	TextDrawColor(TextDrawPunishTitle, -1);
	TextDrawUseBox(TextDrawPunishTitle, true);
	TextDrawBoxColor(TextDrawPunishTitle, -156);
	TextDrawSetShadow(TextDrawPunishTitle, 0);
	TextDrawSetOutline(TextDrawPunishTitle, 1);
	TextDrawBackgroundColor(TextDrawPunishTitle, 51);
	TextDrawFont(TextDrawPunishTitle, 1);
	TextDrawSetProportional(TextDrawPunishTitle, 1);

	TextDrawPunishText = TextDrawCreate(7.000000, 260.166687, "~y~Od: ~w~Denzel Grands~n~~y~Dla: ~w~John Kennedy~n~~y~Powod: ~w~Kosi slupy");
	TextDrawLetterSize(TextDrawPunishText, 0.295499, 1.139165);
	TextDrawTextSize(TextDrawPunishText, 144.500000, -2.333333);
	TextDrawAlignment(TextDrawPunishText, 1);
	TextDrawColor(TextDrawPunishText, -1);
	TextDrawUseBox(TextDrawPunishText, true);
	TextDrawBoxColor(TextDrawPunishText, -216);
	TextDrawSetShadow(TextDrawPunishText, 0);
	TextDrawSetOutline(TextDrawPunishText, 1);
	TextDrawBackgroundColor(TextDrawPunishText, 51);
	TextDrawFont(TextDrawPunishText, 1);
	TextDrawSetProportional(TextDrawPunishText, 1);
}